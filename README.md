## Staged Penetrator Firmware ##

This project contains the firmware source code for the Staged Penetrators affectionately called Gravediggers III & IV.

```
Source Folder description:
./StagedPenetrator.X | MPLABX Project root, contains the Makefile
 + src               | Source code
 |  + init           | Contains the main.c
 |  + config         | Configuration files
 |  + drivers        | Hardware driver layer
 |  + modules        | Firmware operating code and headers
 |  + lib            | Libraries
 |  |  + openlog     | Interfacing with the Sparkfun OpenLog
 |  + scripts        | Pre/Post build scripts for versioning and math.
 |  + util           | Utility programs related to the rockets.
 + nbproject         | Private MPLABX project files, configurations, etc.
```
