#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-penetrator_prod.mk)" "nbproject/Makefile-local-penetrator_prod.mk"
include nbproject/Makefile-local-penetrator_prod.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=penetrator_prod
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/StagedPenetratorHR.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/StagedPenetratorHR.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=src/drivers/pyros.c src/drivers/sensors.c src/drivers/buzzer.c src/init/main.c src/lib/openlog/openlog.c src/modules/eventtimer.c src/modules/flightevent.c src/modules/flightloops.c src/modules/flightstate.c src/modules/globals.c src/modules/isr.c src/modules/logger.c src/modules/math.c src/modules/stringbuffer.c src/modules/beep.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/src/drivers/pyros.p1 ${OBJECTDIR}/src/drivers/sensors.p1 ${OBJECTDIR}/src/drivers/buzzer.p1 ${OBJECTDIR}/src/init/main.p1 ${OBJECTDIR}/src/lib/openlog/openlog.p1 ${OBJECTDIR}/src/modules/eventtimer.p1 ${OBJECTDIR}/src/modules/flightevent.p1 ${OBJECTDIR}/src/modules/flightloops.p1 ${OBJECTDIR}/src/modules/flightstate.p1 ${OBJECTDIR}/src/modules/globals.p1 ${OBJECTDIR}/src/modules/isr.p1 ${OBJECTDIR}/src/modules/logger.p1 ${OBJECTDIR}/src/modules/math.p1 ${OBJECTDIR}/src/modules/stringbuffer.p1 ${OBJECTDIR}/src/modules/beep.p1
POSSIBLE_DEPFILES=${OBJECTDIR}/src/drivers/pyros.p1.d ${OBJECTDIR}/src/drivers/sensors.p1.d ${OBJECTDIR}/src/drivers/buzzer.p1.d ${OBJECTDIR}/src/init/main.p1.d ${OBJECTDIR}/src/lib/openlog/openlog.p1.d ${OBJECTDIR}/src/modules/eventtimer.p1.d ${OBJECTDIR}/src/modules/flightevent.p1.d ${OBJECTDIR}/src/modules/flightloops.p1.d ${OBJECTDIR}/src/modules/flightstate.p1.d ${OBJECTDIR}/src/modules/globals.p1.d ${OBJECTDIR}/src/modules/isr.p1.d ${OBJECTDIR}/src/modules/logger.p1.d ${OBJECTDIR}/src/modules/math.p1.d ${OBJECTDIR}/src/modules/stringbuffer.p1.d ${OBJECTDIR}/src/modules/beep.p1.d

# Object Files
OBJECTFILES=${OBJECTDIR}/src/drivers/pyros.p1 ${OBJECTDIR}/src/drivers/sensors.p1 ${OBJECTDIR}/src/drivers/buzzer.p1 ${OBJECTDIR}/src/init/main.p1 ${OBJECTDIR}/src/lib/openlog/openlog.p1 ${OBJECTDIR}/src/modules/eventtimer.p1 ${OBJECTDIR}/src/modules/flightevent.p1 ${OBJECTDIR}/src/modules/flightloops.p1 ${OBJECTDIR}/src/modules/flightstate.p1 ${OBJECTDIR}/src/modules/globals.p1 ${OBJECTDIR}/src/modules/isr.p1 ${OBJECTDIR}/src/modules/logger.p1 ${OBJECTDIR}/src/modules/math.p1 ${OBJECTDIR}/src/modules/stringbuffer.p1 ${OBJECTDIR}/src/modules/beep.p1

# Source Files
SOURCEFILES=src/drivers/pyros.c src/drivers/sensors.c src/drivers/buzzer.c src/init/main.c src/lib/openlog/openlog.c src/modules/eventtimer.c src/modules/flightevent.c src/modules/flightloops.c src/modules/flightstate.c src/modules/globals.c src/modules/isr.c src/modules/logger.c src/modules/math.c src/modules/stringbuffer.c src/modules/beep.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

# The following macros may be used in the pre and post step lines
Device=PIC18F2523
ProjectDir="C:\Users\Caleb\Development\staged-penetrator-hr\StagedPenetratorHR.X"
ConfName=penetrator_prod
ImagePath="dist\penetrator_prod\${IMAGE_TYPE}\StagedPenetratorHR.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}"
ImageDir="dist\penetrator_prod\${IMAGE_TYPE}"
ImageName="StagedPenetratorHR.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}"
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IsDebug="true"
else
IsDebug="false"
endif

.build-conf:  .pre ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-penetrator_prod.mk dist/${CND_CONF}/${IMAGE_TYPE}/StagedPenetratorHR.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=18F2523
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/src/drivers/pyros.p1: src/drivers/pyros.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/drivers" 
	@${RM} ${OBJECTDIR}/src/drivers/pyros.p1.d 
	@${RM} ${OBJECTDIR}/src/drivers/pyros.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=icd3  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/src/drivers/pyros.p1  src/drivers/pyros.c 
	@-${MV} ${OBJECTDIR}/src/drivers/pyros.d ${OBJECTDIR}/src/drivers/pyros.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/src/drivers/pyros.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/src/drivers/sensors.p1: src/drivers/sensors.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/drivers" 
	@${RM} ${OBJECTDIR}/src/drivers/sensors.p1.d 
	@${RM} ${OBJECTDIR}/src/drivers/sensors.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=icd3  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/src/drivers/sensors.p1  src/drivers/sensors.c 
	@-${MV} ${OBJECTDIR}/src/drivers/sensors.d ${OBJECTDIR}/src/drivers/sensors.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/src/drivers/sensors.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/src/drivers/buzzer.p1: src/drivers/buzzer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/drivers" 
	@${RM} ${OBJECTDIR}/src/drivers/buzzer.p1.d 
	@${RM} ${OBJECTDIR}/src/drivers/buzzer.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=icd3  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/src/drivers/buzzer.p1  src/drivers/buzzer.c 
	@-${MV} ${OBJECTDIR}/src/drivers/buzzer.d ${OBJECTDIR}/src/drivers/buzzer.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/src/drivers/buzzer.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/src/init/main.p1: src/init/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/init" 
	@${RM} ${OBJECTDIR}/src/init/main.p1.d 
	@${RM} ${OBJECTDIR}/src/init/main.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=icd3  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/src/init/main.p1  src/init/main.c 
	@-${MV} ${OBJECTDIR}/src/init/main.d ${OBJECTDIR}/src/init/main.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/src/init/main.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/src/lib/openlog/openlog.p1: src/lib/openlog/openlog.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/lib/openlog" 
	@${RM} ${OBJECTDIR}/src/lib/openlog/openlog.p1.d 
	@${RM} ${OBJECTDIR}/src/lib/openlog/openlog.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=icd3  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/src/lib/openlog/openlog.p1  src/lib/openlog/openlog.c 
	@-${MV} ${OBJECTDIR}/src/lib/openlog/openlog.d ${OBJECTDIR}/src/lib/openlog/openlog.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/src/lib/openlog/openlog.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/src/modules/eventtimer.p1: src/modules/eventtimer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/modules" 
	@${RM} ${OBJECTDIR}/src/modules/eventtimer.p1.d 
	@${RM} ${OBJECTDIR}/src/modules/eventtimer.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=icd3  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/src/modules/eventtimer.p1  src/modules/eventtimer.c 
	@-${MV} ${OBJECTDIR}/src/modules/eventtimer.d ${OBJECTDIR}/src/modules/eventtimer.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/src/modules/eventtimer.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/src/modules/flightevent.p1: src/modules/flightevent.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/modules" 
	@${RM} ${OBJECTDIR}/src/modules/flightevent.p1.d 
	@${RM} ${OBJECTDIR}/src/modules/flightevent.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=icd3  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/src/modules/flightevent.p1  src/modules/flightevent.c 
	@-${MV} ${OBJECTDIR}/src/modules/flightevent.d ${OBJECTDIR}/src/modules/flightevent.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/src/modules/flightevent.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/src/modules/flightloops.p1: src/modules/flightloops.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/modules" 
	@${RM} ${OBJECTDIR}/src/modules/flightloops.p1.d 
	@${RM} ${OBJECTDIR}/src/modules/flightloops.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=icd3  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/src/modules/flightloops.p1  src/modules/flightloops.c 
	@-${MV} ${OBJECTDIR}/src/modules/flightloops.d ${OBJECTDIR}/src/modules/flightloops.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/src/modules/flightloops.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/src/modules/flightstate.p1: src/modules/flightstate.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/modules" 
	@${RM} ${OBJECTDIR}/src/modules/flightstate.p1.d 
	@${RM} ${OBJECTDIR}/src/modules/flightstate.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=icd3  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/src/modules/flightstate.p1  src/modules/flightstate.c 
	@-${MV} ${OBJECTDIR}/src/modules/flightstate.d ${OBJECTDIR}/src/modules/flightstate.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/src/modules/flightstate.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/src/modules/globals.p1: src/modules/globals.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/modules" 
	@${RM} ${OBJECTDIR}/src/modules/globals.p1.d 
	@${RM} ${OBJECTDIR}/src/modules/globals.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=icd3  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/src/modules/globals.p1  src/modules/globals.c 
	@-${MV} ${OBJECTDIR}/src/modules/globals.d ${OBJECTDIR}/src/modules/globals.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/src/modules/globals.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/src/modules/isr.p1: src/modules/isr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/modules" 
	@${RM} ${OBJECTDIR}/src/modules/isr.p1.d 
	@${RM} ${OBJECTDIR}/src/modules/isr.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=icd3  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/src/modules/isr.p1  src/modules/isr.c 
	@-${MV} ${OBJECTDIR}/src/modules/isr.d ${OBJECTDIR}/src/modules/isr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/src/modules/isr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/src/modules/logger.p1: src/modules/logger.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/modules" 
	@${RM} ${OBJECTDIR}/src/modules/logger.p1.d 
	@${RM} ${OBJECTDIR}/src/modules/logger.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=icd3  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/src/modules/logger.p1  src/modules/logger.c 
	@-${MV} ${OBJECTDIR}/src/modules/logger.d ${OBJECTDIR}/src/modules/logger.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/src/modules/logger.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/src/modules/math.p1: src/modules/math.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/modules" 
	@${RM} ${OBJECTDIR}/src/modules/math.p1.d 
	@${RM} ${OBJECTDIR}/src/modules/math.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=icd3  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/src/modules/math.p1  src/modules/math.c 
	@-${MV} ${OBJECTDIR}/src/modules/math.d ${OBJECTDIR}/src/modules/math.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/src/modules/math.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/src/modules/stringbuffer.p1: src/modules/stringbuffer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/modules" 
	@${RM} ${OBJECTDIR}/src/modules/stringbuffer.p1.d 
	@${RM} ${OBJECTDIR}/src/modules/stringbuffer.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=icd3  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/src/modules/stringbuffer.p1  src/modules/stringbuffer.c 
	@-${MV} ${OBJECTDIR}/src/modules/stringbuffer.d ${OBJECTDIR}/src/modules/stringbuffer.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/src/modules/stringbuffer.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/src/modules/beep.p1: src/modules/beep.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/modules" 
	@${RM} ${OBJECTDIR}/src/modules/beep.p1.d 
	@${RM} ${OBJECTDIR}/src/modules/beep.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=icd3  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/src/modules/beep.p1  src/modules/beep.c 
	@-${MV} ${OBJECTDIR}/src/modules/beep.d ${OBJECTDIR}/src/modules/beep.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/src/modules/beep.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
else
${OBJECTDIR}/src/drivers/pyros.p1: src/drivers/pyros.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/drivers" 
	@${RM} ${OBJECTDIR}/src/drivers/pyros.p1.d 
	@${RM} ${OBJECTDIR}/src/drivers/pyros.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/src/drivers/pyros.p1  src/drivers/pyros.c 
	@-${MV} ${OBJECTDIR}/src/drivers/pyros.d ${OBJECTDIR}/src/drivers/pyros.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/src/drivers/pyros.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/src/drivers/sensors.p1: src/drivers/sensors.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/drivers" 
	@${RM} ${OBJECTDIR}/src/drivers/sensors.p1.d 
	@${RM} ${OBJECTDIR}/src/drivers/sensors.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/src/drivers/sensors.p1  src/drivers/sensors.c 
	@-${MV} ${OBJECTDIR}/src/drivers/sensors.d ${OBJECTDIR}/src/drivers/sensors.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/src/drivers/sensors.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/src/drivers/buzzer.p1: src/drivers/buzzer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/drivers" 
	@${RM} ${OBJECTDIR}/src/drivers/buzzer.p1.d 
	@${RM} ${OBJECTDIR}/src/drivers/buzzer.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/src/drivers/buzzer.p1  src/drivers/buzzer.c 
	@-${MV} ${OBJECTDIR}/src/drivers/buzzer.d ${OBJECTDIR}/src/drivers/buzzer.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/src/drivers/buzzer.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/src/init/main.p1: src/init/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/init" 
	@${RM} ${OBJECTDIR}/src/init/main.p1.d 
	@${RM} ${OBJECTDIR}/src/init/main.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/src/init/main.p1  src/init/main.c 
	@-${MV} ${OBJECTDIR}/src/init/main.d ${OBJECTDIR}/src/init/main.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/src/init/main.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/src/lib/openlog/openlog.p1: src/lib/openlog/openlog.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/lib/openlog" 
	@${RM} ${OBJECTDIR}/src/lib/openlog/openlog.p1.d 
	@${RM} ${OBJECTDIR}/src/lib/openlog/openlog.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/src/lib/openlog/openlog.p1  src/lib/openlog/openlog.c 
	@-${MV} ${OBJECTDIR}/src/lib/openlog/openlog.d ${OBJECTDIR}/src/lib/openlog/openlog.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/src/lib/openlog/openlog.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/src/modules/eventtimer.p1: src/modules/eventtimer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/modules" 
	@${RM} ${OBJECTDIR}/src/modules/eventtimer.p1.d 
	@${RM} ${OBJECTDIR}/src/modules/eventtimer.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/src/modules/eventtimer.p1  src/modules/eventtimer.c 
	@-${MV} ${OBJECTDIR}/src/modules/eventtimer.d ${OBJECTDIR}/src/modules/eventtimer.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/src/modules/eventtimer.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/src/modules/flightevent.p1: src/modules/flightevent.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/modules" 
	@${RM} ${OBJECTDIR}/src/modules/flightevent.p1.d 
	@${RM} ${OBJECTDIR}/src/modules/flightevent.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/src/modules/flightevent.p1  src/modules/flightevent.c 
	@-${MV} ${OBJECTDIR}/src/modules/flightevent.d ${OBJECTDIR}/src/modules/flightevent.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/src/modules/flightevent.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/src/modules/flightloops.p1: src/modules/flightloops.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/modules" 
	@${RM} ${OBJECTDIR}/src/modules/flightloops.p1.d 
	@${RM} ${OBJECTDIR}/src/modules/flightloops.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/src/modules/flightloops.p1  src/modules/flightloops.c 
	@-${MV} ${OBJECTDIR}/src/modules/flightloops.d ${OBJECTDIR}/src/modules/flightloops.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/src/modules/flightloops.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/src/modules/flightstate.p1: src/modules/flightstate.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/modules" 
	@${RM} ${OBJECTDIR}/src/modules/flightstate.p1.d 
	@${RM} ${OBJECTDIR}/src/modules/flightstate.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/src/modules/flightstate.p1  src/modules/flightstate.c 
	@-${MV} ${OBJECTDIR}/src/modules/flightstate.d ${OBJECTDIR}/src/modules/flightstate.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/src/modules/flightstate.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/src/modules/globals.p1: src/modules/globals.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/modules" 
	@${RM} ${OBJECTDIR}/src/modules/globals.p1.d 
	@${RM} ${OBJECTDIR}/src/modules/globals.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/src/modules/globals.p1  src/modules/globals.c 
	@-${MV} ${OBJECTDIR}/src/modules/globals.d ${OBJECTDIR}/src/modules/globals.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/src/modules/globals.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/src/modules/isr.p1: src/modules/isr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/modules" 
	@${RM} ${OBJECTDIR}/src/modules/isr.p1.d 
	@${RM} ${OBJECTDIR}/src/modules/isr.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/src/modules/isr.p1  src/modules/isr.c 
	@-${MV} ${OBJECTDIR}/src/modules/isr.d ${OBJECTDIR}/src/modules/isr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/src/modules/isr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/src/modules/logger.p1: src/modules/logger.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/modules" 
	@${RM} ${OBJECTDIR}/src/modules/logger.p1.d 
	@${RM} ${OBJECTDIR}/src/modules/logger.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/src/modules/logger.p1  src/modules/logger.c 
	@-${MV} ${OBJECTDIR}/src/modules/logger.d ${OBJECTDIR}/src/modules/logger.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/src/modules/logger.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/src/modules/math.p1: src/modules/math.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/modules" 
	@${RM} ${OBJECTDIR}/src/modules/math.p1.d 
	@${RM} ${OBJECTDIR}/src/modules/math.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/src/modules/math.p1  src/modules/math.c 
	@-${MV} ${OBJECTDIR}/src/modules/math.d ${OBJECTDIR}/src/modules/math.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/src/modules/math.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/src/modules/stringbuffer.p1: src/modules/stringbuffer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/modules" 
	@${RM} ${OBJECTDIR}/src/modules/stringbuffer.p1.d 
	@${RM} ${OBJECTDIR}/src/modules/stringbuffer.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/src/modules/stringbuffer.p1  src/modules/stringbuffer.c 
	@-${MV} ${OBJECTDIR}/src/modules/stringbuffer.d ${OBJECTDIR}/src/modules/stringbuffer.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/src/modules/stringbuffer.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/src/modules/beep.p1: src/modules/beep.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/modules" 
	@${RM} ${OBJECTDIR}/src/modules/beep.p1.d 
	@${RM} ${OBJECTDIR}/src/modules/beep.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/src/modules/beep.p1  src/modules/beep.c 
	@-${MV} ${OBJECTDIR}/src/modules/beep.d ${OBJECTDIR}/src/modules/beep.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/src/modules/beep.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/StagedPenetratorHR.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) --chip=$(MP_PROCESSOR_OPTION) -G -mdist/${CND_CONF}/${IMAGE_TYPE}/StagedPenetratorHR.X.${IMAGE_TYPE}.map  -D__DEBUG=1 --debugger=icd3  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     --rom=default,-7dc0-7fff --ram=default,-5f4-5ff,-f9c-f9c,-fd4-fd4,-fdb-fdf,-fe3-fe7,-feb-fef,-ffd-fff  -odist/${CND_CONF}/${IMAGE_TYPE}/StagedPenetratorHR.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
	@${RM} dist/${CND_CONF}/${IMAGE_TYPE}/StagedPenetratorHR.X.${IMAGE_TYPE}.hex 
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/StagedPenetratorHR.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) --chip=$(MP_PROCESSOR_OPTION) -G -mdist/${CND_CONF}/${IMAGE_TYPE}/StagedPenetratorHR.X.${IMAGE_TYPE}.map  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"src" -I"src/drivers" -I"src/init" -I"src/config" -I"src/lib/openlog" -I"src/modules" -V --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -odist/${CND_CONF}/${IMAGE_TYPE}/StagedPenetratorHR.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
	
endif

.pre:
	@echo "--------------------------------------"
	@echo "User defined pre-build step: [src\scripts\prebuild.bat -v -b 1 -d 0]"
	@src\scripts\prebuild.bat -v -b 1 -d 0
	@echo "--------------------------------------"

# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/penetrator_prod
	${RM} -r dist/penetrator_prod

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
