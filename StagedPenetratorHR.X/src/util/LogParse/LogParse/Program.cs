﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace LogParse
{
    #region Struct Definitions
    [Flags]
    public enum PyroState
    {
        Arm = 1,
        Fire = 2,
        Jettison = 4
    }

    public enum FlightStatus
    {
        Init = 0,
        OnPad = 1,
        Liftoff = 2,
        Coast = 3,
        Apogee = 4,
        Ignition = 5,
        Jettisoned = 6
    }

    public struct DataPacket
    {
        public UInt32 ElapsedTime;
        public Int32 Acceleration;
        public Int32 Velocity;
        public Int32 Pressure;
        public Int32 MinimumPressure;
        public Int32 Altitude;
        public Int32 EstimatedPressure;
        public Int32 EstimatedMinimumPressure;
        public Int32 EstimatedAltitude;
        public Int32 EstimatedApogee;
        public PyroState Pyros;
        public FlightStatus State;

        override public string ToString()
        {
            return string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}",
                ElapsedTime,
                Acceleration,
                Velocity,
                Pressure,
                MinimumPressure,
                Altitude,
                EstimatedPressure,
                EstimatedMinimumPressure,
                EstimatedAltitude,
                EstimatedApogee,
                (int)Pyros,
                //Pyros.ToString().Replace(@", ", @"+"), 
                (int)State
            );
        }
    }
    #endregion

    public class Program
    {

#if DEBUG
        public static readonly string LOGFILE_PATH = "E:/data.log";
        public static readonly string CSVFILE_PATH = "E:/data.csv";
#else
        public static readonly string LOGFILE_PATH = "./data.log";
        public static readonly string CSVFILE_PATH = "./data.csv";
#endif

        public static readonly string CSV_DATA_HEADER = "time (ms*10),v accel (100*G)," +
                                                        "vel (100*m/s),pres (LSB),pmin (LSB),AGL (ft)," +
                                                        "estP (LSB), estPmin (LSB), estAGL (ft), estApo (ft)," +
                                                        "pyro,status";

        static void Main(string[] args)
        {
            string name = "", ver = "";
            int oneG = 0, gp = 0, el = 0, p1800 = 0, p3600 = 0;

            List<DataPacket> loggedData = new List<DataPacket>();

            // extract data from log file
            using (FileStream logfile = new FileStream(LOGFILE_PATH, FileMode.Open))
            {
                byte sw = (byte)logfile.ReadByte();
                while (logfile.Length > logfile.Position)
                {
                    switch (sw)
                    {
                        case 0x01:
                            ReadData(logfile, loggedData);
                            break;
                        case 0x02:
                            ReadBaseline(logfile, out name, out ver, out oneG, out gp, out el, out p1800, out p3600);
                            break;
                        default:
                            break;
                    }
                    sw = (byte)logfile.ReadByte();
                }
            }

            // write data to CSV
            StringBuilder csv = new StringBuilder();
            string header = string.Format("# Rocket: {0}{2}# Firmware: v{1}{2}", name, ver, Environment.NewLine);
            string baseData = string.Format("# One G: {0} ct{2}# Ground Pressure: {1} LSB{2}# Calculated Elevation: {3} ft{2}# Min P_thres: {4} LSB, Max P_thres: {5} LSB{2}",
                oneG, gp, Environment.NewLine, el, p1800, p3600);

            csv.Append(header);
            csv.Append(baseData);
            csv.Append(CSV_DATA_HEADER);
            csv.Append(Environment.NewLine);

            foreach (DataPacket p in loggedData)
            {
                csv.Append(p.ToString());
                csv.Append(Environment.NewLine);
            }

            try
            {
                File.WriteAllText(CSVFILE_PATH, csv.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine("There was a problem writing the CSV file.");
                Console.WriteLine("Close any programs using it and try again.{0}{0}Press 'm' for " +
                    "more details and any other key to quit.", Environment.NewLine);

                Console.Write(">");
                if (Console.ReadKey().KeyChar == 'm')
                {
                    Console.Write(Environment.NewLine + e.Message);
                    Console.ReadKey();
                }
            }
        }

        static void ReadBaseline(FileStream logfile,
            out string name,
            out string ver,
            out int oneG,
            out int gp,
            out int el,
            out int p1800,
            out int p3600)
        {
            int size = logfile.ReadByte();

            ReadInt32(logfile, out oneG);
            ReadInt32(logfile, out gp);
            ReadInt32(logfile, out el);
            ReadInt32(logfile, out p1800);
            ReadInt32(logfile, out p3600);
            ReadString(logfile, out name);
            ReadString(logfile, out ver);
        }

        static void ReadData(FileStream logfile, List<DataPacket> loggedData)
        {
            int size = logfile.ReadByte();
            DataPacket pkt;
            int pyros, state;

            ReadUint32(logfile, out pkt.ElapsedTime);
            ReadInt32(logfile, out pkt.Acceleration);
            ReadInt32(logfile, out pkt.Velocity);
            ReadInt32(logfile, out pkt.Pressure);
            ReadInt32(logfile, out pkt.MinimumPressure);
            ReadInt32(logfile, out pkt.Altitude);
            ReadInt32(logfile, out pkt.EstimatedPressure);
            ReadInt32(logfile, out pkt.EstimatedMinimumPressure);
            ReadInt32(logfile, out pkt.EstimatedAltitude);
            ReadInt32(logfile, out pkt.EstimatedApogee);
            ReadByte(logfile, out pyros);
            ReadByte(logfile, out state);

            pkt.Pyros = (PyroState)pyros;
            pkt.State = (FlightStatus)state;

            loggedData.Add(pkt);
        }

        #region FileStream Reading Utilities
        static void ReadUint32(FileStream logfile, out UInt32 data)
        {
            byte[] raw = new byte[4];
            logfile.Read(raw, 0, 4);
            data = (UInt32)((raw[0] << 24) | (raw[1] << 16) | (raw[2] << 8) | (raw[3]));
        }

        static void ReadInt32(FileStream logfile, out Int32 data)
        {
            byte[] raw = new byte[4];
            logfile.Read(raw, 0, 4);
            data = ((raw[0] << 24) | (raw[1] << 16) | (raw[2] << 8) | (raw[3]));
        }

        static void ReadInt16(FileStream logfile, out int data)
        {
            byte[] raw = new byte[2];
            logfile.Read(raw, 0, 2);
            data = ((raw[0] << 8) | (raw[1]));
        }

        static void ReadByte(FileStream logfile, out int data)
        {
            data = logfile.ReadByte();
        }

        static void ReadString(FileStream logfile, out string data)
        {
            StringBuilder b = new StringBuilder();
            byte c = (byte)logfile.ReadByte();
            while (c != '\0')
            {
                b.Append((char)c);
                c = (byte)logfile.ReadByte();
            }
            data = b.ToString();
        }
        #endregion
    }
}
