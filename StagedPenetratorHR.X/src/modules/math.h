/* 
 * math.h
 * Caleb Moore
 * 2014-08-14
 *
 * Math utilities.
 */

#ifndef MATH_H
#define	MATH_H

#define LOOKUP_TABLE_SIZE    (128)
#define LOOKUP_OUT_OF_BOUNDS ((int32_t)0xFFFF)

// fast absolute value calculation
#define MATH_ABS(x) (x ^ (x >> (sizeof x - 1)) - (x >> (sizeof x - 1)))

// sensor calibration
#define MATH_ACCELEROMETER_TRANS_OFFSET 0L
#define MATH_ACCELEROMETER_TRANS_NUM    -100L
#define MATH_ACCELEROMETER_TRANS_DEN    31L
//#define MATH_ACCELEROMETER_TRANS_DEN    125L

#define MATH_GEE_IN_MS2_X100            981L
#define MATH_INTEGRATION_SCALE_FACTOR   100000L

#include <xc.h>
#include <stdint.h>

void integrate(int32_t *out, int32_t f1, int32_t f2, int32_t dt);
void fractionalIntegrate(int32_t *out, int32_t f1, int32_t f2, int32_t dt, int32_t num, int32_t den);
void linearTransform(int32_t *out, int32_t offset, int32_t scale);
void linearFractionalTransform(int32_t *out, int32_t offset, int32_t num, int32_t den);
int32_t linearInterpolation(int32_t x0, int32_t x1, int32_t y0, int32_t y1, int32_t x);

/**
 * Calculates altitude in ft (MSL) from the passed pressure in ADC counts.
 *
 * @param pres in ADC counts (0 - 4095)
 * @return corresponding altitude MSL
 */
int32_t getAltitudeFromPressure(int32_t p);

/**
 * Calculates pressure in ADC counts from the passed altitude in ft. Based on
 * the US Standard Atmosphere.
 *
 * @param altitude in ft MSL
 * @return corresponding pressure in ADC counts
 */
int32_t getPressureFromAltitude(int32_t z);

void getAccelerationFromADC(int32_t *adc);

#endif	/* MATH_H */

