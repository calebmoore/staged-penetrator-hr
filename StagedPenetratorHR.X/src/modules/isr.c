#include <xc.h>
#include "isr.h"
#include <usart.h>
#include "globals.h"
#include "eventtimer.h"
#include "openlog.h"
#include "sensors.h"
#include <adc.h>

/**
 * Kept short and fast. No function calls, no priorities.
 *
 * A brief outline of the ISR:
 *
 *   * reset Timer1 on overflow
 *   * signal to rest of code if ADC conversion is done
 *   * on TX interrupt activation, continuously send the byte in TXREG until NUL
 *   * on RC interrupt, change status of the OpenLog depending on received char
 */
void __interrupt(high_priority) isr() {
    unsigned char rxChar;

    // event timer overflow handling
    if (PIR1bits.TMR1IF) {
        PIR1bits.TMR1IF = 0;

        // reset timer
        TMR1H = EVENT_TIMER_PERIOD_10MS >> 8;
        TMR1L = EVENT_TIMER_PERIOD_10MS;

        eventTimer.ticks++;
        flags.eventTick = 1;
    }

    // ADC conversion done
    if (PIR1bits.ADIF) {
        PIR1bits.ADIF = 0;

        sensorsHandleSample();
    }

    // USART transmit interrupt
    if (PIR1bits.TXIF) {
        while(!TXSTAbits.TRMT); // wait until USART is ready

        if(!strbufIsEmpty(&logBuffer)) {
            // empty the string in the transmit buffer, one interrupt at a time
            TXREG = strbufReadByte(&logBuffer);
        } else {
            // let the rest of the code know the logger is busy
            PIE1bits.TXIE = 0;
            if (openlogState == kOpenLogStateSendingData)
                openlogState = kOpenLogStateLogging;
        }
    }

    // USART receive interrupt
    if (PIR1bits.RCIF) {
        rxChar = RCREG;

        if (rxChar == (unsigned char)OPENLOG_LOGGING_READY) {
            openlogState = kOpenLogStateLogging;
        } else if (rxChar == (unsigned char)OPENLOG_COMMAND_READY) {
            openlogState = kOpenLogStateCommand;
        }
    }
}
