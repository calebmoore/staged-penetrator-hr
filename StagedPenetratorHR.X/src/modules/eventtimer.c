#include "eventtimer.h"
#include "globals.h"
#include <timers.h>

// global reference
volatile struct EventTimer eventTimer;

static FlightEvent *events[EVENT_TIMER_N_EVENTS];
static uint8_t nEvents;

/**
 * Initializes the event timer.
 */
void eventTimerInit() {
    eventTimer.ticks = 0;
    nEvents = 0;
}

/**
 * Sets Timer1's period and configures Timer1 for a 1:1 prescale in 16 bit mode.
 */
void eventTimerStart() {
    WriteTimer1(EVENT_TIMER_PERIOD_10MS);

    OpenTimer1(TIMER_INT_ON  &
               T1_16BIT_RW   &
               T1_SOURCE_INT &
               T1_PS_1_1     &
               T1_OSC1EN_ON  &
               T1_SOURCE_CCP);
}

/**
 * Stops the event timer.
 */
void eventTimerStop() {
    CloseTimer1();
}

/**
 * Adds the passed flight event to the calling list of the event timer.
 *
 * @param event reference to event
 * @return the number of subscribed event
 */
uint8_t eventTimerSubscribe(FlightEvent *event) {
    if (nEvents >= EVENT_TIMER_N_EVENTS)
        return EVENT_TIMER_SUBSCRIPTION_FAIL;
    
    events[nEvents] = event;

    // return the index of the subscribed event as confirmation
    return nEvents++;
}

/**
 * Iterates through all subscribed events, decrements their counters, and calls
 * any triggered callbacks.
 */
void eventTimerDoEvents() {
    FlightEvent *e;
    uint8_t i = 0;
    int16_t newTicks;

    // wait for an event tick
    if (!flags.eventTick)
        return;

    flags.eventTick = 0;

    // Iterate through all events. If the event is still active, either its
    // counter is decremented, or its callback is called.
    for (i = 0; i < nEvents; i++) {
        e = events[i];
        if (e->ticks == FLIGHT_EVENT_INACTIVE) {
            continue;
        } else if (--(e->ticks) == 0) {   
            newTicks = e->event(); // run the callback
            e->ticks = newTicks ? newTicks : e->period; // set/reset period
        }
    }
}
