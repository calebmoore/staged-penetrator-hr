/* 
 * globals.h
 * Caleb Moore
 * 2014-08-08
 *
 * Global utilities and state flags.
 */

#ifndef GLOBALS_H
#define	GLOBALS_H

#include <xc.h>
#include <stdint.h>
#include "eventtimer.h"
#include "flightstate.h"
#include "flightloops.h"

#define _XTAL_FREQ          14745600L // crystal frequency

// bit shift & register utility definitions
#define PORT_PIN(port, pin) PORT ## port ## pin
#define PORT(port)          PORT ## port
#define LAT_PIN(port, pin)  LAT ## port ## pin
#define LAT(port)           LAT ## port
#define BITS(reg)           reg ## bits
#define REG_HIGH(reg, mask) reg |= (mask)
#define REG_LOW(reg, mask)  reg &= ~(mask)

struct Flags {
    uint8_t eventTick:1;         // 10 ms has passed
    uint8_t adcSampleReady:1;   // a single ADC conversion is ready
    uint8_t adcConversionDone:1; // full ADC conversion is ready
    uint8_t pyrosArmed:1;        // pyros electrically connected to comp
    uint8_t launched:1;          // rocket is off the pad
    uint8_t meco:1;              // main engine cutoff; booster burnout
    uint8_t apogee:1;            // rocket has reached highest point
    uint8_t ignited:1;           // outboard motors ignited
    uint8_t jettisoned:1;        // computer is ejected
    uint8_t safeVelocity:1;      // rocket is traveling well below Mach 1
};

extern volatile struct Flags flags;
extern struct FlightState flightState;
extern FlightLoop flightLoop;

#endif	/* GLOBALS_H */

