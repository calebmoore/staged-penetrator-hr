/*
 * File:   beep.h
 * Author: Winglee Plasma Lab
 *
 * Created on November 21, 2014, 4:18 PM
 */

#ifndef BEEP_H
#define	BEEP_H

#include <xc.h>
#include <stdint.h>
#include "buzzer.h"

#define BEEP_OFF() BUZZER_LAT &= ~(1 << BUZZER_PIN)
#define BEEP_ON()  BUZZER_LAT |=  (1 << BUZZER_PIN)

#define BEEP_TYPE_OFF         ((int8_t)0)
#define BEEP_TYPE_ON          ((int8_t)1)
#define BEEP_TYPE_CMD         ((int8_t)-1)
#define BEEP_LENGTH_SHORT     ((int8_t)10)
#define BEEP_LENGTH_MED       ((int8_t)50)
#define BEEP_LENGTH_LONG      ((int8_t)100)
#define BEEP_LENGTH_OBNOXIOUS ((int8_t)300)
#define BEEP_SEQ_REPEAT       ((int8_t)1)
#define BEEP_SEQ_STOP         ((int8_t)0)

typedef struct Beep {
    int8_t beepType;
    int16_t beepLength;
} Beep;

extern Beep _seqInit[];
extern Beep _seqOnPad[];
extern Beep _seqArmed[];
extern Beep _seqFire[];
extern Beep _seqFindMe[];
extern Beep _seqNone[];
extern Beep _seqNoSD[];

#endif	/* BEEP_H */
