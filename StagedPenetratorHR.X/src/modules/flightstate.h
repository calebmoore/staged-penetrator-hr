/* 
 * flightstate.h
 * Caleb Moore
 * 2014-08-13
 *
 * Data structure holding current flight status and sensor data.
 */

#ifndef FLIGHTSTATE_H
#define	FLIGHTSTATE_H

#include <xc.h>
#include <stdint.h>

/**
 * What phase of the flight the rocket is in.
 */
enum FlightStatus {
    kFlightStatusInit = 0,
    kFlightStatusOnPad = 1,
    kFlightStatusLiftoff = 2,
    kFlightStatusCoast = 3,
    kFlightStatusApogee = 4,
    kFlightStatusIgnition = 5,
    kFlightStatusJettisoned = 6
};

struct FlightThresholds {
    int32_t minimumArmPres;             // minimum pyro arming altitude/pres
    int32_t maximumIgnPres;             // upper bound for ign delay calculation
    int32_t minimumIgnPres;             // lower bound for ign delay calculation
    int32_t mainChutePres;              // altitude to deploy main
};

struct FlightState {
    uint16_t oneGee;                    // one g offset
    int32_t acceleration;               // current acceleration in g/100
    int32_t velocity;                   // current velocity in cm/s
    int32_t pressure;                   // current pressure in raw ADC counts
    int32_t groundPressure;             // ground pressure offset for AGL calc
    int32_t minimumPressure;            // minimum recorded pressure
    int32_t estimatedPressure;          // estimated real pressure
    int32_t estimatedMinimumPressure;   // estimated real minimum pressure
    int32_t estimatedApogee;            // maximum height
    int32_t elevation;                  // approx. elevation of launch pad
    int32_t altitude;                   // approx altitude AGL, in ft.
    int32_t estimatedAltitude;           // estimated real altitude AGL in ft.
    uint8_t pyroState;                  // current state of the pyro channels
    enum FlightStatus status;           // current flight loop
    struct FlightThresholds thresholds; // flight event pressure thresholds
};

void flightStateInit(struct FlightState *state);

#endif	/* FLIGHTSTATE_H */

