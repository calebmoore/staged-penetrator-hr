#include "logger.h"
#include "math.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "globals.h"
#include "openlog.h"
#include "flightevent.h"
#include "eventtimer.h"
#include "sensors.h"
#include "build.h"
#include "timing.h"


#define LOG_FILE      "data.log"
#define DATA_HEADER   "time (ms/10),v accel (100*G)," \
                      "vel (100*m/s),pres (ct),pmin (ct),pyro,status"

#define LOG_PACKET_TYPE_DATA   (uint8_t)0x1
#define LOG_PACKET_TYPE_BASE   (uint8_t)0x2

typedef struct DataPacket {
    uint8_t type;
    uint8_t size;
    uint8_t *data;
} DataPacket;

// events
static FlightEvent _logData;

// event callbacks
static int16_t logData(void);

// static functions
static void logInitialData(void);
#ifndef ASCII_LOGGING
void formatData(unsigned char *data);
static void addInt32(unsigned char *buf, int32_t data);
static void addInt16(unsigned char *buf, int16_t data);
static void addByte(unsigned char *buf, uint8_t data);
static void addString(unsigned char *buf, const char* data);
#endif

/**
 * Initializes the data logger
 */
void loggerInit() {
    openlogInit();

#ifndef OPENLOG_START_IN_CMD_MODE
    openlogEnterCommandMode();
#endif

    openlogRemoveFile(LOG_FILE);
    openlogAppendFile(LOG_FILE);
    
    logInitialData();

    initializeFlightEvent(&_logData, LOG_PERIOD, logData);
    eventTimerSubscribe(&_logData);
}

/**
 * Prints the passed string from program memory to the logging output.
 *
 * @param data data to log
 */
void log(const char *data) {
    openlogPrintln(data);
}

/**
 * Writes the referenced packet to the logging output in binary format.
 *
 * @param data reference to the packet to write
 */
void logPacket(DataPacket *data) {
    int i;

    openlogPrintByte(data->type);
    openlogPrintByte(data->size);
    for (i = 0; i < data->size; i++)
        openlogPrintByte(data->data[i]);

    openlogWriteData();
}

#ifdef ASCII_LOGGING
/**
 * Plaintext data logging for debugging.
 */
static int16_t logData(void) {
    unsigned char ac[10], v[16], p[10], mp[10], s[10], agl[10];
    unsigned char estp[10], estmp[10], estagl[10];

    ltoa(ac, flightState.acceleration, 10);
    //ltoa(v, flightState.velocity, 10);
    //ltoa(p, flightState.pressure, 10);
    //ltoa(mp, flightState.minimumPressure, 10);
    ltoa(s, (int32_t)flightState.status, 10);
    //ltoa(agl, flightState.altitude, 10);
    ltoa(estp, flightState.estimatedPressure, 10);
    ltoa(estmp, flightState.estimatedMinimumPressure, 10);
    ltoa(estagl, flightState.estimatedAltitude, 10);

    openlogPrint("A: ");
    openlogPrint(ac);
    openlogPrint(" ");

//    openlogPrint("V: ");
//    openlogPrint(v);
//    openlogPrint(" ");
//
//    openlogPrint("P: ");
//    openlogPrint(p);
//    openlogPrint(" ");
//
//    openlogPrint("AGL: ");
//    openlogPrint(agl);
//    openlogPrint(" ");

//    openlogPrint("MP: ");
//    openlogPrint(mp);
//    openlogPrint(" ");

    openlogPrint("EP: ");
    openlogPrint(estp);
    openlogPrint(" ");

    openlogPrint("EMP: ");
    openlogPrint(estmp);
    openlogPrint(" ");

    openlogPrint("EAGL: ");
    openlogPrint(estagl);
    openlogPrint(" ");

    openlogPrint("S: ");
    openlogPrint(s);
    openlogPrintln("");

    return FLIGHT_EVENT_CONTINUE;
}

/**
 * Log initial data and rocket information.
 */
static void logInitialData() {
    int el, p1800, p3600;
    unsigned char og[8], gp[8], eStr[8], p18Str[8], p36Str[8];

    ltoa(og, flightState.oneGee, 10);
    ltoa(gp, flightState.groundPressure, 10);

    el = (int)getAltitudeFromPressure(flightState.groundPressure);
    p1800 = getPressureFromAltitude(el + 1800);
    p3600 = getPressureFromAltitude(el + 3600);

    itoa(eStr, el, 10);
    itoa(p18Str, p1800, 10);
    itoa(p36Str, p3600, 10);

    openlogPrintln(ROCKET_NAME);
    openlogPrint("Firmware v");
    openlogPrintln(VERSION_STR);

    openlogPrint("One G: ");
    openlogPrint(og);
    openlogPrintln(" LSB");

    openlogPrint("Ground Pres: ");
    openlogPrint(gp);
    openlogPrintln(" LSB");

    openlogPrint("Elevation: ");
    openlogPrint(eStr);
    openlogPrintln(" ft");

    openlogPrint("Pres @1800ft: ");
    openlogPrint(p18Str);
    openlogPrintln(" LSB");

    openlogPrint("Pres @3600ft: ");
    openlogPrint(p36Str);
    openlogPrintln(" LSB");
}
#else
/**
 * Logs sensor and flight state data to the connected USART device.
 */
static int16_t logData(void) {
    DataPacket packet;
    unsigned char data[128] = { 0 };
    formatData(data);

    packet.type = LOG_PACKET_TYPE_DATA;
    packet.size = *data;
    packet.data = data + 1;

    logPacket(&packet);
    
    return FLIGHT_EVENT_CONTINUE;
}

/**
 * Log inital data and rocket information.
 */
static void logInitialData() {
    unsigned char data[64] = { 0 };
    DataPacket baseline;
    baseline.type = LOG_PACKET_TYPE_BASE;

    addInt32(data, flightState.oneGee);
    addInt32(data, flightState.groundPressure);
    addInt32(data, flightState.elevation);
    addInt32(data, flightState.thresholds.minimumIgnPres);
    addInt32(data, flightState.thresholds.maximumIgnPres);
    addString(data, ROCKET_NAME);
    addString(data, VERSION_STR);

    baseline.size = *data;
    baseline.data = data + 1;

    logPacket(&baseline);
}

/** 
 * Add sensor/flight data to the referenced stream.
 */
void formatData(unsigned char *buf) {
    addInt32(buf, eventTimer.ticks - LOG_PERIOD);
    addInt32(buf, flightState.acceleration);
    addInt32(buf, flightState.velocity);
    addInt32(buf, flightState.pressure);
    addInt32(buf, flightState.minimumPressure);
    addInt32(buf, flightState.altitude);
    addInt32(buf, flightState.estimatedPressure);
    addInt32(buf, flightState.estimatedMinimumPressure);
    addInt32(buf, flightState.estimatedAltitude);
    addInt32(buf, flightState.estimatedApogee);
    addByte(buf, (uint8_t)flightState.pyroState);
    addByte(buf, (uint8_t)flightState.status);
}

/**
 * Add a long to a data stream.
 *
 * @param buf  binary data stream to use
 * @param data long to write
 */
static void addInt32(unsigned char *buf, int32_t data) {
    int i;
    int size;

    i = sizeof(int32_t) - 1;
    do {
        size = *buf;
        buf[size+1] = (uint8_t)(data >> 8*i);
        (*buf)++;
    } while (i--);
}


/**
 * Add an int to a data stream.
 *
 * @param buf  binary data stream to use
 * @param data int to write
 */
static void addInt16(unsigned char *buf, int16_t data) {
    int i;
    int size;
    for (i = sizeof(int16_t)-1; i == 0; i--) {
        size = *buf;
        buf[size+1] = (uint8_t)(data >> 8*i);
        (*buf)++;
    }
}

/**
 * Add an ASCII string to a data stream.
 *
 * @param buf  binary data stream to use
 * @param data string to write
 */
static void addString(unsigned char *buf, const char* data) {
    int size;
    do {
        size = *buf;
        buf[size+1] = *data;
        (*buf)++;
    } while (*data++);
}

/**
 * Add a single byte to a data stream.
 *
 * @param buf  binary data stream to use
 * @param data byte to write
 */
static void addByte(unsigned char *buf, uint8_t data) {
    buf[*buf+1] = data;
    (*buf)++;
}
#endif
