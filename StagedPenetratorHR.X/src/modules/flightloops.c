/*
 * loops.c
 * Caleb Moore
 * 2014-08-25
 *
 * Definitions for all flight loops.
 */
#include "flightloops.h"
#include "globals.h"
#include "flightevent.h"
#include "flightstate.h"
#include "eventtimer.h"
#include "thresholds.h"
#include "timing.h"
#include "pyros.h"
#include "build.h"
#include "logger.h"
#include "math.h"

// private prototypes
static void loopOnPad(void);
static void loopLiftoff(void);
static void loopCoast(void);
static void loopIgnition(void);
static void loopJettisoned(void);
static int16_t burnBabyBurn(uint16_t delay);

// penetrator specific functions
#ifdef IS_PENETRATOR
static void loopApogee(void);
static FlightEvent _apogeeCheck;
static int16_t checkForApogee(void);
static uint16_t calculateDelay(void);

#   ifdef EJECT_BOOSTER
static int16_t ejectBooster(uint16_t delay);
static FlightEvent _ejectBooster;
#   endif
#endif

// events
static FlightEvent _velocityCheck;
static FlightEvent _mecoCheck;
static FlightEvent _burnBabyBurn; // yeeehaw

// event callbacks
static int16_t checkVelocity(void);
static int16_t checkForMeco(void);

/**
 * Sets initial status of the rocket and loads the next flight loop.
 */
void loopInit() {
    flightState.status = kFlightStatusInit;

    flightLoop = loopOnPad;
}

/**
 * Waits for launch, moves over to loopLiftoff if it's detected.
 */
static void loopOnPad() {
    flightState.status = kFlightStatusOnPad;

//    flightState.velocity = 0;
    if (flightState.acceleration > (int32_t)LAUNCH_ACCELERATION) {
        flags.launched = 1;
        flightLoop = loopLiftoff;
    }
}

/**
 * Schedules a check to make sure the rocket is going fast enough to be
 * considered in flight. If not, it was a glitch, and it resets to the
 * launchpad.
 *
 * The velocity check event callback handles switching flight loops.
 */
static void loopLiftoff() {
    static int isScheduled = 0;
    
    flightState.status = kFlightStatusLiftoff;

    if (!isScheduled) {
        initializeFlightEvent(&_velocityCheck,
                              VELOCITY_CHECK_DELAY,
                              checkVelocity);
        isScheduled = eventTimerSubscribe(&_velocityCheck);
    } else if (_velocityCheck.ticks == FLIGHT_EVENT_INACTIVE) {
        // Reset event period. No sense creating a new event.
        _velocityCheck.ticks = _velocityCheck.period;
    }
}

/**
 * Looks for booster stage burnout. When it occurs, transfers to apogee handling
 * after running safety checks.
 */
static void loopCoast() {
    static int isMecoCheckScheduled = 0;
#ifdef IS_PENETRATOR
    static int isApogCheckScheduled = 0;
#endif

    flightState.status = kFlightStatusCoast;

    // wait for the booster to finish before looking for any thresholds
    if (!flags.meco) {
        if (!isMecoCheckScheduled) {
            initializeFlightEvent(&_mecoCheck, MECO_DETECT_PERIOD, checkForMeco);
            isMecoCheckScheduled = eventTimerSubscribe(&_mecoCheck);
        }
        return;
    }

    if (!flags.pyrosArmed) {
#ifdef IS_PENETRATOR

#ifndef IGNORE_VELOCITY
        if (!flags.safeVelocity)
            return; // the rocket is going too fast to trust the pressure sensor
#endif
        // arm the pyros if above minimum safe altitude
        if (flightState.estimatedAltitude >= (int32_t)MINIMUM_ARM_ALT)
            armPyros();
        return;
#else
        // ignite second stage
        armPyros();
        burnBabyBurn(SECOND_STAGE_IGN_DELAY);
#endif
    }
    
#ifdef IS_PENETRATOR
    // flags.apogee is false until the _apogeeCheck event detects a
    // preconfigured number of consecutive increases in pressure
    if (!flags.apogee) {
        if (!isApogCheckScheduled) {
            initializeFlightEvent(&_apogeeCheck,
                                  APOGEE_DETECT_PERIOD,
                                  checkForApogee);
            isApogCheckScheduled = eventTimerSubscribe(&_apogeeCheck);
        }
        return;
    }

    flightLoop = loopApogee;
#else
    if (flags.ignited)
        flightLoop = loopIgnition;
#endif
}

#ifdef IS_PENETRATOR
/**
 * Calculates the proper delay before igniting the outboards and schedules
 * outboard ignition based on minimum recorded pressure.
 */
static void loopApogee() {
    static int scheduled = 0;
    uint16_t delay;

    flightState.status = kFlightStatusApogee;

    // schedule ignition based on max altitude (minimum pressure)
    if (!scheduled) {
        delay = calculateDelay();

#ifdef EJECT_BOOSTER
        // eject the booster just before ignition
        ejectBooster(delay - BOOSTER_EJECT_DELAY);
#endif

        scheduled = burnBabyBurn(calculateDelay());
    }

    if (flags.ignited)
        flightLoop = loopIgnition;
}
#endif

/**
 * Blows the chute for the computer, after the motors have ignited.
 */
static void loopIgnition() {
    flightState.status = kFlightStatusIgnition;

    // wait for igniters to stop
    if (flightState.pyroState & PYROS_IGN)
        return;

#ifndef EJECT_BOOSTER
#   ifndef IS_TIPOVER
    // dump the computer
    if (!flags.jettisoned)
        jettisonComputer();
#   else
    // fire main at proper altitude
    if (!flags.jettisoned) {
        if (flightState.pressure > flightState.thresholds.mainChutePres)
            jettisonComputer();
        else
            return;
    }
#   endif
    
    // wait for jettison igniter to stop
    if (flightState.pyroState & PYROS_JET)
        return;
#endif
    
    flightLoop = loopJettisoned;
}

/**
 * Disarms everything.
 */
static void loopJettisoned() {
    flightState.status = kFlightStatusJettisoned;

    disarmPyros();
}

/**
 * Ignite the outboards after the passed delay.
 *
 * @param delay
 * @return event id
 */
static int16_t burnBabyBurn(uint16_t delay) {
    if (delay == 0)
        delay = 1; // period of 0 when scheduling disables the event before it runs

    // schedule ignition
    initializeFlightEvent(&_burnBabyBurn, delay, igniteMotors);
    return eventTimerSubscribe(&_burnBabyBurn);
}

#ifdef EJECT_BOOSTER
static int16_t ejectBooster(uint16_t delay) {
    if (delay <= 0)
        delay = 1;

    // schedule ejection
    initializeFlightEvent(&_ejectBooster, delay, jettisonComputer);
    return eventTimerSubscribe(&_ejectBooster);
}
#endif

/**
 * Searches for the booster stage burnout by waiting for negative acceleration.
 *
 * @return whether or not to continue the event
 */
static int16_t checkForMeco() {
    static int ct = MECO_DETECT_COUNT;
    if (flightState.acceleration < 0) {
        if (!ct--)
            flags.meco = 1;
    } else {
        ct = MECO_DETECT_COUNT;
    }

    return flags.meco ? FLIGHT_EVENT_INACTIVE : FLIGHT_EVENT_CONTINUE;
}

#ifdef IS_PENETRATOR
/**
 * Detects apogee by ensuring pressure is increasing.
 *
 * @return whether the rocket has reached apogee
 */
static int16_t checkForApogee() {
    static int ct = APOGEE_DETECT_COUNT;
    
    if ((flightState.estimatedApogee - flightState.estimatedAltitude) >= APOGEE_DELTA_Z) {
        if (!ct--)
            flags.apogee = 1;
    } else {
        ct = APOGEE_DETECT_COUNT;
    }

    return flags.apogee ? FLIGHT_EVENT_INACTIVE : FLIGHT_EVENT_CONTINUE;
}
#endif

#ifdef IS_PENETRATOR
/**
 * Calculates the ignition delay after detecting apogee, based on the height
 * reached.
 *
 * @return delay in sec*100
 */
static uint16_t calculateDelay() {
    int32_t delay;

#ifndef IS_TIPOVER
    if (flightState.estimatedAltitude > (int32_t)MAXIMUM_IGN_ALT) {
        delay = (int32_t)MAX_IGN_DELAY;
    } else {
        delay = linearInterpolation(
                    (int32_t)MINIMUM_IGN_ALT, (int32_t)MIN_IGN_DELAY,
                    (int32_t)MAXIMUM_IGN_ALT, (int32_t)MAX_IGN_DELAY,
                    flightState.estimatedAltitude
                );
    }
#else
    delay = CONSTANT_IGN_DELAY;
#endif

    return (uint16_t)delay;
}
#endif

#ifdef IGNORE_VELOCITY
/**
 * Used when velocity is to be ignored for debugging purposes. Switches to the
 * next loop, and deactivates.
 *
 * @return message to disable the event
 */
static int16_t checkVelocity() {
    flightLoop = loopCoast;
    return FLIGHT_EVENT_INACTIVE;
}
#else

/**
 * One time event. Checks to make sure the rocket is actually flying after a
 * launch detection event.
 *
 * If it is, program execution continues normally. If it isn't, program returns
 * to loopOnPad.
 *
 * @return message to disable the event
 */
static int16_t checkVelocity() {
    if (flightState.velocity < MINIMUM_VELOCITY) {
        flags.launched = 0;
        flightState.velocity = 0;

        flightLoop = loopOnPad;
    } else {
        flightLoop = loopCoast;
    }

    return FLIGHT_EVENT_INACTIVE;
}
#endif
