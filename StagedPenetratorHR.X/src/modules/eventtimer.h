/* 
 * eventtimer.h
 * Caleb Moore
 * 2014-08-08
 *
 * Millisecond timer for time-critical event scheduling.
 */

#ifndef EVENT_TIMER_H
#define	EVENT_TIMER_H

#include <xc.h>
#include <stdint.h>
#include <timers.h>
#include "flightevent.h"

#define EVENT_TIMER_PERIOD_10MS 0x7000 // Timer1 value, period 10 ms @14.7456MHz
#define EVENT_TIMER_N_EVENTS    32     // max number of events
#define EVENT_TIMER_SUBSCRIPTION_FAIL 255 // response if an event isn't added

struct EventTimer {
    uint32_t ticks;
};

extern volatile struct EventTimer eventTimer;

void eventTimerInit(void);
void eventTimerStart(void);
void eventTimerStop(void);
uint8_t eventTimerSubscribe(FlightEvent *event);
void eventTimerDoEvents(void);

#endif	/* MILLI_H */
