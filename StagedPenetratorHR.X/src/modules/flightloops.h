/* 
 * flightloops.h
 * Caleb Moore
 * 2014-08-25
 *
 * The flight loop is called as often as possible (ie, NOT at a regular period),
 * and controls the flight profile of the rocket. Different loops perform
 * different tasks, such as igniting motors and deploying parachutes.
 *
 * There is one globally available reference to the current loop. This reference
 * is changed to the next appropriate loop whenever a loop has finished all of
 * its tasks.
 */

#ifndef FLIGHTLOOPS_H
#define	FLIGHTLOOPS_H

#include <xc.h>
#include <stdint.h>

/**
 * Function pointer to current flight loop.
 */
typedef void (*FlightLoop)(void);

/**
 * Performs initialization and places the rocket on the launch pad.
 */
void loopInit(void);

#endif	/* FLIGHTLOOPS_H */

