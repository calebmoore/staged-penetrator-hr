/* 
 * File:   circularbuffer.h
 * Author: Winglee Plasma Lab
 *
 * A circular buffer built to hold strings. For the most part, it abstracts away
 * all of C18's annoying string handling and data managing.
 *
 * Use the normally named (e.g. "strbufWriteString") functions for string
 * constants and string literals, use the "-Ram" suffixed
 * (e.g. strbufWriteStringRam) functions for runtime initiated/filled character
 * arrays.
 *
 * The size of the buffer MUST be a power of 2 in order for it to function
 * correctly. Modulo operations are expensive, so bitwise ANDs are used instead
 * for calculating index positions. A consequence of this is that the maximum
 * number of characters the buffer can hold is (size - 1).
 *
 * Since the buffer is initiated with a preexisting character array, the size of
 * the array must be greater than or equal to the size passed into strbufInit().
 *
 * Example usage:
 *
 *     const unsigned char size 128;
 *     char *buffer[size];
 *     char *data[20];
 *
 *     struct StringBuffer sb;
 *
 *     strbufInit(&sb, buffer, size);
 *     strbufWriteString("A");
 *
 *     itoa(123, data);
 *     strbufWriteStringRam(data);
 *
 *     strbufReadChar(); // returns 'A'
 *     strbufReadChar(); // returns '1'
 *
 * Created on August 6, 2014, 11:23 AM
 */

#ifndef STRINGBUFFER_H
#define	STRINGBUFFER_H

#include <xc.h>
#include <stdint.h>

struct StringBuffer {
    volatile char *buffer;
    volatile uint8_t start;
    volatile uint8_t end;
    size_t size;
};

/*
 * Initializes the referenced StringBuffer.
 */
void strbufInit(struct StringBuffer *sb, 
                volatile char *buffer,
                size_t size);

/*
 * Writes a single character to the referenced buffer.
 */
void strbufWriteByte(struct StringBuffer *sb, char data);

/*
 * Writes the entire passed string to the referenced buffer, not including
 * the NUL character.
 */
void strbufWriteString(struct StringBuffer *sb, const char *data);

/*
 * Writes the entire passed ram string to the referenced buffer, not including
 * the NUL character.
 */
void strbufWriteRamString(struct StringBuffer *sb, const char *data);

/*
 * Returns the character at the beginning of the buffer.
 */
char strbufReadByte(struct StringBuffer *sb);

/*
 * Whether the buffer is empty.
 */
uint8_t strbufIsEmpty(struct StringBuffer *sb);

/*
 * Whether the buffer is completely full.
 */
uint8_t strbufIsFull(struct StringBuffer *sb);

#endif	/* STRINGBUFFER_H */
