#include "flightevent.h"

void initializeFlightEvent(FlightEvent *event,
                           uint16_t period,
                           FlightEventCallback callback) {
    event->period = period;
    event->ticks = period;
    event->event = callback;
}


