/* 
 * flightevents.h
 * Caleb Moore
 * 2014-08-13
 *
 * A FlightEvent is a repeated action called at a regular interval. They are
 * used in conjuction with the EventTimer, using the eventTimerSubscribe
 * function.
 */

#ifndef FLIGHTEVENT_H
#define	FLIGHTEVENT_H

// callback return messages
#define FLIGHT_EVENT_INACTIVE -1 // disables the event
#define FLIGHT_EVENT_CONTINUE 0  // use the calling period given at init time

#include <xc.h>
#include <stdint.h>

/**
 * Reference to the function called on each event trigger.
 *
 * Returns either the the desired time before being called again, in 10ths of a
 * second, EVENT_TIMER_INACTIVE to disable the event, or EVENT_TIMER_CONTINUE
 * to keep the same calling period.
 */
typedef int16_t (*FlightEventCallback)(void);

typedef struct FlightEvent {
    uint16_t period;       // the desired period, in 10ths of a second
    uint16_t ticks;        // event ticks until the event is called
    FlightEventCallback event; // callback function
} FlightEvent;

/**
 * Initializes the referenced FlightEvent.
 *
 * @param event    FlightEvent struct to fill
 * @param period   how often to perform the event, in 10ths of a second
 * @param callback action to perform
 */
void initializeFlightEvent(FlightEvent *event,
                           uint16_t period,
                           FlightEventCallback callback);

#endif	/* FLIGHTEVENT_H */
