/* 
 * isr.h
 * Caleb Moore
 * 2014-08-01
 *
 * The ugliest ISR I've ever had the displeasure of writing.
 *
 * Function calls are entirely avoided, since with C18, ANY function call in the
 * ISR, whether it gets called or not, triggers a worst case context save.
 *
 * Interrupt priorities are also disabled, since they are poorly handled as
 * well.
 *
 * See <http://www.xargs.com/pic/c-faq.html#c18isr> (questions 14-16) for more
 * info.
 *
 * Please forgive me.
 */

#ifndef ISR_H
#define	ISR_H

#endif	/* ISR_H */
