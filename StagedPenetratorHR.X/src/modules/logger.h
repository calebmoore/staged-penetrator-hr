/* 
 * logger.h
 * Caleb Moore
 * 2014-08-26
 *
 * Provides high-speed, binary or ASCII logging of flight events and sensor
 * data to an SD card (OpenLogger) or debug serial connection.
 */

#ifndef LOGGER_H
#define	LOGGER_H

#include <xc.h>
#include <stdint.h>

void loggerInit(void);
void log(const char *data);
void logInt16(int16_t data);
void logInt32(int32_t data);

#endif	/* LOGGER_H */

