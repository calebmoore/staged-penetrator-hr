#include "math.h"
#include "sensors.h"

/**
 * Successive trapezoidal approximation

 * @param f1  value 1
 * @param f2  value 2
 * @param dt  time between values
 * @param out running sum
 */
void integrate(int32_t *out, int32_t f1, int32_t f2, int32_t dt) {
    fractionalIntegrate(out, f1, f2, dt, 1L, 1L);
}

/**
 * Successive trapezoidal approximation with scalar multiple out front,
 * ie:
 *  (num/den)*int[f dt]
 *
 * @param f1  value 1
 * @param f2  value 2
 * @param dt  time between values
 * @param out running sum
 */
void fractionalIntegrate(int32_t *out, int32_t f1, int32_t f2, int32_t dt, int32_t num, int32_t den) {
    *out += (((f1/2) + (f2/2) + (f1 & f2 & 1)) * dt * num) / den;
}

/**
 * Applies a transorm with the passed properties.
 *
 * @param out    where to put the result
 * @param offset linear offset
 * @param scale  scale factor
 */
void linearTransform(int32_t *out, int32_t offset, int32_t scale) {
    *out = (*out - offset) * scale;
}

/**
 * Applies a transform with the passed properties.
 *
 * @param out    where to put the result
 * @param offset linear offset
 * @param num    scale factor numerator
 * @param den    scale factor denominator
 */
void linearFractionalTransform(int32_t *out, int32_t offset, int32_t num, int32_t den) {
    *out = (*out - offset) * num / den;
}

int32_t getAltitudeFromPressure(int32_t p) {
    static const int32_t bucketWidth = (1 << ADC_EQU_RESOLUTION) / LOOKUP_TABLE_SIZE;
    static const int32_t maxIndexValue = (1 << ADC_EQU_RESOLUTION) - LOOKUP_TABLE_SIZE;

    // 128 values of the US Standard Atmosphere, from 0 to 15 psi.
    // 
    // There's a singularity around an ADC count of about 500. This is due to
    // the way the pressure sensor is calibrated: it will never rail under
    // normal operation since 0 psi registers somewhere around 0.6 V.
    static const int32_t lookup[LOOKUP_TABLE_SIZE] = {
        64186L, 65434L, 66771L, 68212L, 69778L, 71495L, 73401L, 75549L, 78021L,
        80953L, 84595L, 89504L, 97512L, 100819,  82770L, 75117L, 69924L, 65915L,
        62619L, 59801L, 57330L, 55123L, 53124L, 51294L, 49604L, 48032L, 46561L,
        45178L, 43871L, 42633L, 41454L, 40330L, 39255L, 38225L, 37235L, 36282L,
        35363L, 34475L, 33617L, 32786L, 31981L, 31199L, 30439L, 29700L, 28980L,
        28279L, 27595L, 26928L, 26276L, 25640L, 25017L, 24408L, 23811L, 23227L,
        22654L, 22093L, 21542L, 21001L, 20470L, 19949L, 19437L, 18933L, 18438L,
        17951L, 17471L, 17000L, 16535L, 16077L, 15627L, 15183L, 14745L, 14313L,
        13887L, 13467L, 13053L, 12644L, 12241L, 11842L, 11449L, 11060L, 10676L,
        10297L, 9922L, 9552L, 9186L, 8824L, 8466L, 8111L, 7761L, 7415L, 7072L,
        6733L, 6397L, 6065L, 5736L, 5410L, 5087L, 4768L, 4452L, 4138L, 3828L,
        3520L, 3216L, 2914L, 2614L, 2318L, 2024L, 1732L, 1443L, 1157L, 872L,
        591L, 311L, 34L, -241L, -514L, -785L, -1053L, -1320L, -1584L, -1846L,
        -2107L, -236L, -2622L, -2877L, -3129L, -3380L, -3630L
    };

    int32_t z0, z1, p0, p1;

    if (p > maxIndexValue)
        // things don't divide quite evenly enough
        return LOOKUP_OUT_OF_BOUNDS;
    else if (p & (bucketWidth - 1)) {
        // bucketize passed pressure
        p0 = p / bucketWidth;
        p1 = p / bucketWidth + 1;
        z0 = lookup[p0];
        z1 = lookup[p1];

        return linearInterpolation(bucketWidth*p0, z0, bucketWidth*p1, z1, p);
    } else {
        // p happens to fall on an index
        return lookup[p / bucketWidth];
    }
}

/**
 * Accurately calculates the pressure (in ADC counts) corresponding to a given
 * altitude (in feet). Only values up to 12,600 feet are supported currently.
 *
 * @param z altitude to find pressure for
 * @return calculated pressure, or 0 if requested altitude is over 12600 ft.
 */
int32_t getPressureFromAltitude(int32_t z) {
    static const int32_t bucketWidth = (1 << ADC_EQU_RESOLUTION) / LOOKUP_TABLE_SIZE;
    static const int32_t maxIndexValue = (1 << ADC_EQU_RESOLUTION) - LOOKUP_TABLE_SIZE;

    // pressure values of the US Standard Atmosphere spaced in 100 ft intervals
    // (ie, 0ft - 12600ft)
    static const int32_t lookup[LOOKUP_TABLE_SIZE] = {
        14479L, 14420L, 14361L, 14302L, 14243L, 14185L, 14127L, 14069L, 14011L,
        13953L, 13896L, 13839L, 13782L, 13725L, 13669L, 13613L, 13557L, 13501L,
        13445L, 13390L, 13335L, 13280L, 13225L, 13170L, 13116L, 13062L, 13008L,
        12954L, 12901L, 12848L, 12794L, 12742L, 12689L, 12636L, 12584L, 12532L,
        12480L, 12428L, 12377L, 12326L, 12275L, 12224L, 12173L, 12122L, 12072L,
        12022L, 11972L, 11922L, 11873L, 11824L, 11774L, 11725L, 11677L, 11628L,
        11580L, 11532L, 11484L, 11436L, 11388L, 11341L, 11293L, 11246L, 11200L,
        11153L, 11106L, 11060L, 11014L, 10968L, 10922L, 10877L, 10831L, 10786L,
        10741L, 10696L, 10651L, 10607L, 10563L, 10518L, 10474L, 10431L, 10387L,
        10344L, 10300L, 10257L, 10214L, 10172L, 10129L, 10087L, 10044L, 10002L,
        9960L, 9919L, 9877L, 9836L, 9794L, 9753L, 9713L, 9672L, 9631L, 9591L,
        9551L, 9511L, 9471L, 9431L, 9391L, 9352L, 9313L, 9274L, 9235L, 9196L,
        9158L, 9119L, 9081L, 9043L, 9005L, 8967L, 8929L, 8892L, 8855L, 8817L,
        8780L, 8744L, 8707L, 8670L, 8634L, 8598L, 8562L, 8526L
    };

    int32_t z0, z1, p0, p1;

    if (z > maxIndexValue || z < 0)
        // values this high aren't actually supported (yet?)
        return LOOKUP_OUT_OF_BOUNDS;
    else if (z & (bucketWidth - 1)) {
        // bucketize passed pressure
        p0 = z / bucketWidth;
        p1 = p0 + 1;
        z0 = lookup[p0];
        z1 = lookup[p1];

        return linearInterpolation(bucketWidth*p0, z0, bucketWidth*p1, z1, z);
    } else {
        // z happens to fall on an index
        return lookup[z / bucketWidth];
    }
}

int32_t linearInterpolation(int32_t x0, int32_t y0, int32_t x1, int32_t y1, int32_t x) {
    static int32_t scale = 100L;
    int32_t y;

    // scale up to keep accuracy
    int32_t xs = scale*x;
    int32_t x0s = scale*x0;
    int32_t x1s = scale*x1;
    int32_t y0s = scale*y0;
    int32_t y1s = scale*y1;

    // interpolate
    y = y0s + (y1s - y0s)*(xs - x0s)/(x1s - x0s);

    return (y / scale);
}

void getAccelerationFromADC(int32_t *adc) {
    static int32_t adcMaxVal = (1 << ADC_EQU_RESOLUTION) - 1;
    static int32_t scaledRefV = ADC_REFERENCE_V * 10000L;
    
    linearFractionalTransform(adc, 0L, scaledRefV, adcMaxVal * 10); // sensor reading in mV
    linearFractionalTransform(adc, 0L, MATH_ACCELEROMETER_TRANS_NUM, 37); // converted to g/100
}
