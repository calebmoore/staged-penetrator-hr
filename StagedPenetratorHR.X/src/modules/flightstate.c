#include <stdio.h>
#include "globals.h"
#include "flightstate.h"
#include "sensors.h"
#include "math.h"
#include "logger.h"
#include "thresholds.h"

/**
 * Take baseline measurements.
 *
 * @param state FlightState to stuff
 */
static void takeBaselineData(struct FlightState *state);

/**
 * Instantiates all values in the FlightState referenced.
 *
 * @param state
 */
void flightStateInit(struct FlightState *state) {
    state->oneGee = 0L;
    state->acceleration = 0L;
    state->pyroState = 0L;
    state->pressure = 0L;
    state->groundPressure = 0L;
    state->minimumPressure = 0x7fffffff;
    state->estimatedPressure = 0L;
    state->estimatedMinimumPressure = 0x7ffffff;
    state->status = kFlightStatusInit;
    state->velocity = 0L;
    state->elevation = 0L;
    state->altitude = 0L;
    state->estimatedAltitude = 0L;
    state->estimatedApogee = 0L;

    takeBaselineData(state);
}

/**
 * Initializes the flight state with baseline sensor data.
 *
 * @param state FlightState to stuff
 */
static void takeBaselineData(struct FlightState *state) {
    int32_t oneG = 0L;
    int32_t pres = 0L;
    int i = 16;

    // take average of 16 measurements
    while (i--) {
        oneG += (int32_t)sensorsReadChannel(SENSORS_ACCELEROMETER);
        pres += (int32_t)sensorsReadChannel(SENSORS_PRESSURE);
    }
    oneG = oneG >> 4;
    pres = pres >> 4;

    state->oneGee = oneG;
    state->groundPressure = pres;
    state->pressure = pres;
    state->estimatedPressure = pres;

    state->elevation = getAltitudeFromPressure(pres);

    // calculate pressure at important altitudes
    state->thresholds.minimumArmPres =
            getPressureFromAltitude(state->elevation + MINIMUM_ARM_ALT);
    state->thresholds.maximumIgnPres =
            getPressureFromAltitude(state->elevation + MAXIMUM_IGN_ALT);
    state->thresholds.minimumIgnPres =
            getPressureFromAltitude(state->elevation + MINIMUM_IGN_ALT);
    state->thresholds.mainChutePres =
            getPressureFromAltitude(state->elevation + MAIN_CHUTE_ALT);
}
