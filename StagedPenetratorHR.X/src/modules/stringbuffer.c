#include "stringbuffer.h"

/*
 * Linkes the passed character array to the referenced StringBuffer, and
 * initializes the indices.
 */
void strbufInit(struct StringBuffer *sb,
                volatile char *buffer,
                size_t size) {
    sb->buffer = buffer;
    sb->start = 0;
    sb->end = 0;
    sb->size = size;
}

/*
 * Places the passed byte in the buffer and adjusts indices.
 */
void strbufWriteByte(struct StringBuffer *sb, char data) {
    sb->buffer[sb->end] = (char)data;
    sb->end = (sb->end + 1) & (sb->size - 1);
    if (sb->end == sb->start)
        sb->start = (sb->start + 1) & (sb->size - 1);
}

/*
 * Places the referenced string in the buffer, up to but not including the
 * NUL character.
 */
void strbufWriteString(struct StringBuffer *sb, const char *data) {
    while (*data != '\0') {
        strbufWriteByte(sb, *data++);
    }
}

/*
 * Places the referenced ram string in the buffer, up to but not including the
 * NUL character.
 *
 * You'll notice the code is identical to strbufWriteString, save for the
 * pointer type of the passed string. Yeah. Thanks, C18.
 */
void strbufWriteRamString(struct StringBuffer *sb, const char *data) {
    while (*data != '\0') {
        strbufWriteByte(sb, *data++);
    }
}

/*
 * Reads and removes the char at the beginning of the buffer and adjusts
 * indices.
 */
char strbufReadByte(struct StringBuffer *sb) {
    char data = sb->buffer[sb->start];
    sb->start = (sb->start + 1) & (sb->size - 1);
    return data;
}

/**
 * Whether the string buffer is empty.
 *
 * @param sb buffer to check
 * @return 1 if empty, 0 otherwise
 */
uint8_t strbufIsEmpty(struct StringBuffer *sb) {
    return sb->end == sb->start;
}

/**
 * Whether the string buffer is full.
 *
 * @param sb buffer to check
 * @return 1 if full, 0 otherwise
 */
uint8_t strbufIsFull(struct StringBuffer *sb) {
    return (sb->end + 1) & (sb->size - 1) == sb->start;
}
