/* 
 * main.c
 * Caleb Moore
 * 2014-8-8
 *
 * Main entry point for flight firmware.
 */

#include <xc.h>
#include <adc.h>
#include <stdint.h>
#include "config.h"
#include "eventtimer.h"
#include "openlog.h"
#include "globals.h"
#include "sensors.h"
#include "flightevent.h"
#include "flightstate.h"
#include "pyros.h"
#include "logger.h"
#include "buzzer.h"

// flight events
static FlightEvent _debugClk;

// event callbacks
static int16_t toggleClk(void);

/**
 * Configures TRIS registers and interrupts along with calling each module's
 * initiation routine.
 */
void init(void);

/**
 * Handles asynchronous sampling, oversampling, and decimation of the ADC.
 */
void handleADC(void);

/*
 * Runs initial configuration, then starts the main execution loop.
 * 
 * ADC conversions are handled when available, as signaled by the ISR.
 * Scheduled FlightEvents are then run, followed by the current global
 * flight loop.
 *
 * FlightEvents are typically defined in their modules and scheduled when
 * needed. The flight loops are defined in loops.c, and referenced in globals.h.
 */
int main() {
    init();

    while(1) {
        // handle ADC conversions as they happen
        handleADC();
        
        // perform scheduled events
        eventTimerDoEvents();

        // main flight control loop
        flightLoop();
    }

    return 0;
}

/**
 * Runs through and initializes every flight module, configuring outputs and
 * taking initial data to prepare for flight operation. In order:
 *
 *     eventTimer : 10 ms timer used to schedule time critical processes
 *     pyros      : safing/firing of black powder charges and motor igniters
 *     loops      : main flight loops and their associated FlightEvents
 *     sensors    : ADC conversions of onboard sensors and storage of their data
 *     flightState: struct holding data and current status of the rocket
 *     logger     : periodically logs current data to an SD card
 */
void init() {
    int t;
    TXREG = 0xff; // load dummy char

    buzzerDisable(); // beeps on start up when it shouldn't

    // disable interrupt priorities, enable peripheral and global interrupts
    RCONbits.IPEN = 0;
    INTCONbits.PEIE = 1;

    // ~3 sec delay to let things stabilize
    for (t = 0; t < 200; t++)
        __delay_ms(15);

    eventTimerInit();

    // debug clock, toggles a pin every time the event clock triggers
    TRISBbits.TRISB4 = 0;
    initializeFlightEvent(&_debugClk, 1, toggleClk);
    eventTimerSubscribe(&_debugClk);

    pyrosInit();
    loopInit();

    // initiate ADC module and take initial data
    sensorsInit();
    
    // get ready to accept data
    flightStateInit(&flightState);

    // enable global interrupts
    INTCONbits.GIE = 1;

    buzzerInit();
    buzzerEnable();

    loggerInit();

    // here we go
    eventTimerStart();    
}

void handleADC() {
    if (flags.adcConversionDone) {
        flags.adcConversionDone = 0;
        sensorsHandleFullConversion();
    }
}

/**
 * Toggle an output pin in time with the event timer to diagnose timing errors
 * and to make sure processes aren't too resource hungry.
 *
 * When looked at with a scope, the output should be a perfect 100 Hz (10ms
 * period), 5V square wave.
 *
 * @return message to keep the same event timing
 */
static int16_t toggleClk() {
    LATBbits.LATB4 ^= 1;
    return FLIGHT_EVENT_CONTINUE;
}