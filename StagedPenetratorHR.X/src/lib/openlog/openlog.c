#include "openlog.h"
#include <usart.h>
#include <string.h>
#include "globals.h"

// private prototypes
static void reset(void);
static void writeBuffer(void);
static void writeCommand(void);

static const char kCmdCommandMode[] = { 26, 26, 26, 0};
static const char kCmdNewFile[]     = "new ";
static const char kCmdAppendFile[]  = "append ";
static const char kCmdRemoveFile[]  = "rm ";
static const char kCmdNewLine[]     = "\r";
static const char kDataNewLine[]    = "\r\n";

static volatile char txBuffer_[OPENLOG_TX_BUFFER_SIZE] = { 0 };

struct StringBuffer logBuffer; // global status struct
volatile enum OpenLogState openlogState;

/**
 * Opens USART communication at 38400 baud, enables TX/RC interrupts, resets
 * the OpenLog, and initializes the transmit buffer. 
 *
 * The USART module is configured for asynchronous communication, eight bits per
 * word, without addressing. The baud rate generator is operating in 16 bit
 * mode.
 */
void openlogInit() {
    openlogState = kOpenLogStateOff;

    OPENLOG_PORT_CONFIGURE();

    PIE1bits.TXIE = 0;
    PIE1bits.RCIE = 1;

    // start USART
    baudUSART(BAUD_IDLE_CLK_LOW &
              BAUD_AUTO_OFF     &
              BAUD_IDLE_RX_PIN_STATE_HIGH &
              BAUD_16_BIT_RATE);

    OpenUSART(USART_TX_INT_OFF  &
              USART_RX_INT_ON   &
              USART_ASYNCH_MODE &
              USART_EIGHT_BIT   &
              USART_CONT_RX     &
              USART_ADDEN_OFF   &
              USART_BRGH_HIGH,
              OPENLOG_BAUD_GEN_VAL_115200_14MHZ);

    strbufInit(&logBuffer, txBuffer_, OPENLOG_TX_BUFFER_SIZE);

    reset(); // prepare the OpenLog for RX.
}

/**
 * Sends three escape chars to the OpenLog to put it in command mode and waits
 * for a response.
 */
void openlogEnterCommandMode() {
    if (openlogState != kOpenLogStateLogging)
        return;

    strbufWriteString(&logBuffer, kCmdCommandMode);
    writeBuffer();

    while (openlogState != kOpenLogStateCommand);
}

/*
 * Creates a new file with the passed name. OpenLog must be in command mode
 * and the filename must be a string literal or constant.
 */
void openlogNewFile(const char *file) {
    if (openlogState != kOpenLogStateCommand)
        return;

    strbufWriteString(&logBuffer, kCmdNewFile);
    strbufWriteString(&logBuffer, file);
    writeCommand();

    while (openlogState != kOpenLogStateCommand);
}

/*
 * Removes a file with the passed name. OpenLog must be in command mode
 * and the filename must be a string literal or constant.
 */
void openlogRemoveFile(const char *file) {
    if (openlogState != kOpenLogStateCommand)
        return;

    strbufWriteString(&logBuffer, kCmdRemoveFile);
    strbufWriteString(&logBuffer, file);
    writeCommand();

    while (openlogState != kOpenLogStateCommand);
}

/*
 * Sends the command to open the passed file and transitions to logging mode.
 */
void openlogAppendFile(const char *file) {
    if (openlogState != kOpenLogStateCommand)
        return;

    strbufWriteString(&logBuffer, kCmdAppendFile);
    strbufWriteString(&logBuffer, file);
    writeCommand();

    while(openlogState != kOpenLogStateLogging);
}

/*
 * Adds the passed string to the transmit buffer.
 */
void openlogPrint(const char *data) {
    if (openlogState != kOpenLogStateLogging)
        return;

    strbufWriteString(&logBuffer, data);
}

/*
 * Adds the passed string to the buffer and immediately writes to the OpenLog.
 */
void openlogPrintln(const char *data) {
    if (openlogState != kOpenLogStateLogging)
        return;
    strbufWriteString(&logBuffer, data);
    strbufWriteString(&logBuffer, kDataNewLine);
    openlogWriteData();
}

void openlogPrintByte(uint8_t data) {
    if (openlogState != kOpenLogStateLogging)
        return;
    strbufWriteByte(&logBuffer, (char)data);
}

/**
 * Turns the OpenLog off and on again, and waits until it's ready for data.
 */
static void reset() {
//    if (openlogState == kOpenLogStateCommand) {
//        strbufWriteString(&logBuffer, kCmdCmdReset);
//        strbufWriteString(&logBuffer, kDataNewLine);
//        writeCommand();
//    } else {
    int i = 100;

    OPENLOG_DISABLE();

    // ~0.25 second delay 
    while (i--) {
        __delay_ms(25); 
    }

    OPENLOG_ENABLE();

#ifdef OPENLOG_START_IN_CMD_MODE
    while(openlogState != kOpenLogStateCommand);
#else
    while(openlogState != kOpenLogStateLogging);
#endif
}

/*
 * Enables USART TX interrupts, triggering the USART module to start sending
 * data. This is handled in the ISR, and TX interrupts are disabled after all
 * data is sent.
 */
static void writeBuffer() {
    PIE1bits.TXIE = 1;
}

/*
 * Appends the TX buffer with the appropriate newline to signify a command being
 * sent, and writes the command.
 */
static void writeCommand() {
    openlogState = kOpenLogStateSendingCommand;
    strbufWriteString(&logBuffer, kCmdNewLine);
    writeBuffer();
}

/*
 * Sends any data in the buffer.
 */
void openlogWriteData() {
    openlogState = kOpenLogStateSendingData;
    writeBuffer();
}
