/* 
 * File:   openlog.h
 * Author: Caleb Moore
 *
 * This library supports the Sparkfun OpenLog.
 * <https://github.com/sparkfun/OpenLog>
 *
 * The SD card used in the OpenLog should contain the following in its
 * config.txt:
 *
 *     115200,26,3,0,0,0baud,escape,esc#,mode,verb,echo
 *
 * This configures the OpenLog to operate at 38400 baud, with Ctrl-Z (ASCII 26)
 * as it's escape char. It requires 3 repeated escape chars to enter command
 * mode. It defaults to logging mode, with verbose and echo modes turned off.
 * More information can be found on the wiki:
 * <https://github.com/sparkfun/OpenLog/wiki/>
 *
 * Transmission and reception of data is performed asynchronously with the TX
 * and RC interrupts, handled in isr.c.
 *
 * Since, by default, constant strings are stored in program memory (ROM), most
 * functions are set up for predefined strings and string literals. A seperate
 * set of functions, suffixed by "Ram," are to be used for strings stored in
 * data memory (RAM), such as runtime created character arrays.
 */

#ifndef OPENLOG_H
#define	OPENLOG_H

#include <xc.h>
#include <stdint.h>
#include "stringbuffer.h"

#define OPENLOG_TX_BUFFER_SIZE      128 // size of tx buffer in bytes

/*
 * Baudrate generation values. See datasheet for equation.
 *
 * Values vary based on desired baudrate and CPU clockspeed. Make sure the
 * OpenLog config file is set to agree with the selected baudrate.
 *
 * All of the values at 14.7456 MHz produce a baudrate with a 0.000% error.
 */
#define OPENLOG_BAUD_GEN_VAL_38400_16MHZ  103 // 38400 baud, 16Mhz
#define OPENLOG_BAUD_GEN_VAL_57600_16MHZ  68  // 57600 baud, 16Mhz
#define OPENLOG_BAUD_GEN_VAL_115200_16MHZ 34  // 115200 baud, 16Mhz
#define OPENLOG_BAUD_GEN_VAL_38400_14MHZ  95  // 38400 baud, 14.7456 MHz
#define OPENLOG_BAUD_GEN_VAL_57600_14MHZ  63  // 57600 baud, 14.7456 MHz
#define OPENLOG_BAUD_GEN_VAL_115200_14MHZ 31  // 115200 baud, 14.7456 MHz

/*
 * When defined, the OpenLog is expected to be configured to start up in
 * command mode, meaning no manual commands will be sent to place it there.
 */
#define OPENLOG_START_IN_CMD_MODE

#define OPENLOG_PORT_CONFIGURE()          TRISBbits.TRISB5 = 0; \
                                          LATBbits.LATB5 = 0
#define OPENLOG_DISABLE()                 LATBbits.LATB5 = 1
#define OPENLOG_ENABLE()                  LATBbits.LATB5 = 0

#define OPENLOG_LOGGING_READY '<'  // the OpenLog is ready for data
#define OPENLOG_COMMAND_READY '>'  // the OpenLog is ready for commands

/*
 * The current state of the OpenLog.
 */
enum OpenLogState {
    kOpenLogStateOff,
    kOpenLogStateCommand,
    kOpenLogStateLogging,
    kOpenLogStateSendingCommand,
    kOpenLogStateSendingData
};

extern struct StringBuffer logBuffer;     // global tx buffer
extern volatile enum OpenLogState openlogState; // global logger status

/*
 * Starts the PIC's USART module and prepares the OpenLog for use.
 */
void openlogInit(void);

/*
 * Puts the OpenLog in command mode, enabling it to create and delete files on
 * the SD card. Also enables manual configuration of behaviours.
 */
void openlogEnterCommandMode(void);

/*
 * When in command mode, creates the passed file in the root directory.
 */
void openlogNewFile(const char *file);

/*
 * When in command mode, removes the passed file from the root directory.
 */
void openlogRemoveFile(const char *file);

/*
 * When in command mode, opens the passed file and places itself in logging
 * mode. New data is then appended to the end of the file.
 *
 * NOTE: It seems the OpenLog firmware is buggy, and command mode is
 * inaccessible if a file is open. Don't use this until ready.
 */
void openlogAppendFile(const char *file);

/*
 * When in command mode, opens the passed file and places itself in logging
 * mode. New data is then appended to the end of the file. The filename must be
 * stored in data memory.
 *
 * NOTE: It seems the OpenLog firmware is buggy, and command mode is
 * inaccessible if a file is open. Don't use this until ready.
 */
void openlogAppendFileRam(const char *file);

/*
 * When in logging mode, logs the passed string.
 */
void openlogPrint(const char *file);

/*
 * When in logging mode, logs the passed string and starts a new line.
 */
void openlogPrintln(const char *data);

/*
 * When in logging mode, logs the passed string. The string must be located in
 * data memory.
 */
void openlogPrintRam(const char *data);

/*
 * When in logging mode, logs the passed string and starts a new line. The
 * string must be located in data memory.
 */
void openlogPrintlnRam(const char *data);

void openlogPrintByte(uint8_t data);

void openlogWriteData(void);

#endif	/* OPENLOG_H */

