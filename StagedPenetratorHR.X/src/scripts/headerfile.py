from datetime import datetime

class HeaderFile:
    """Container for a C header file allowing the reading and writing of #defines."""

    DEFINE_TOKEN = "#define"
    IFNDEF_TOKEN = "#ifndef"
    ENDIF_TOKEN = "#endif"
    UNDEF_TOKEN = "#undef"

    MACRO_FORMAT_STRING = "{0} {1} {2}\n" 

    def __init__(self, header_file):
        self._define_macros = {}
        self._undefine_macros = []
        self._file_location = header_file
        self._name = ""

        self._get_defines(self)


    def get_macro_names(self):
        """Returns the define macro names from the header file."""
        return self._define_macros.keys()


    def get_macro_value(self, key):
        """Returns the value of the passed macro name."""
        return self._define_macros[key]


    def get_macros(self):
        return self._define_macros.copy()


    def set(self, key, value):
        self._define_macros[key] = value

    def define(self, key):
        self._define_macros[key] = ""

    def undefine(self, key):
        self._undefine_macros.append(key)

    def save(self):
        with open(self._file_location, "w") as header:
            self._write(header)


    def _get_defines(self, header_file):
        """Extracts define macros from the header file."""
        with open(self._file_location, "r") as header:
            for line in header:
                self._extract(line)


    def _extract(self, line):
        """Parses the macro and value from the passed line."""
        tokens = line.split()
        is_define = len(tokens) > 0 and tokens[0] == self.DEFINE_TOKEN
        is_ifndef = len(tokens) > 0 and tokens[0] == self.IFNDEF_TOKEN
        has_value = len(tokens) > 2

        if is_define and has_value:
            self._define_macros[tokens[1]] = "".join(tokens[2:])
        elif is_ifndef and len(self._name) == 0:
            self._name = tokens[1]


    def _write(self, header):
        self._doc_comment(header)
        self._define_guard(header)
        self._write_macros(header)
        self._end_file(header)
    

    def _doc_comment(self, header):
        header.write("/**\n")
        header.write(" * " + header.name.split('/')[-1] + "\n")
        header.write(" * \n")
        header.write(" * Generated: " + str(datetime.now()) + " by robot.\n")
        header.write(" */\n\n")


    def _define_guard(self, header):
        header.write(self.IFNDEF_TOKEN + " " + self._name + "\n")
        header.write(self.DEFINE_TOKEN + " " + self._name + "\n\n")


    def _write_macros(self, header):
        for name in self._define_macros:
            val = self._define_macros[name]

            if val is None:
                continue
            elif isinstance(val, (int, long)):
                pass
            elif len(str(val)) > 0:
                val = "\"" + str(val) + "\""
            else: 
                val = ""

            header.write(self.MACRO_FORMAT_STRING.format(self.DEFINE_TOKEN, name, val))

        for name in self._undefine_macros:
            header.write(self.MACRO_FORMAT_STRING.format(self.UNDEF_TOKEN, name, ""))


    def _end_file(self, header):
        header.write("\n" + self.ENDIF_TOKEN + " /* " + self._name + " */\n")