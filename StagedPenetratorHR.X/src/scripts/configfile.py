import json

class ConfigFile:
    """Stores config information for the firmware."""

    _config = {}
    _json_file = ""

    def __init__(self, json_file):
        self._json_file = json_file
        self._config = self._extract(json_file)

    
    def _extract(self, json_file):
        """Loads data from the config file."""

        obj = {}
        with open(json_file) as j:
            obj = json.load(j)
        return obj

    def set(self, obj):
        self._config = obj

    def save(self):
        """Writes the config file back to the given location"""
        with open(self._json_file, "w") as j:
            json.dump(self._config, j, sort_keys=True, indent=4, separators=(',', ': '))

    def __getitem__(self, key):
        return self._config[key]

    def __setitem__(self, key, value):
        self._config[key] = value