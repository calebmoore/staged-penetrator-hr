class Version:

    major = 0
    minor = 0
    revision = 0
    build = 0

    def __init__(self, obj):
        self.major = obj["major"]
        self.minor = obj["minor"]
        self.revision = obj["revision"]
        self.build = obj["build"]

    def __str__(self):
        return "{0}.{1}.{2}-{3}".format(self.major, self.minor, self.revision, self.build)

    def getObject(self):
        return {
            "major": self.major,
            "minor": self.minor,
            "revision": self.revision,
            "build": self.build
        }

    def inc(self, val):
        # if no version increment, increment build number
        if val is None:
            return
        elif val == "build":
            self.build += 1
        else:
            self.build = 1

            # increment version as appropriate
            if val == "maj":
                self.major, self.minor, self.revision = self.major + 1, 0, 0
            elif val == "min":
                self.minor, self.revision = self.minor + 1, 0
            elif val == "rev":
                self.revision += 1
