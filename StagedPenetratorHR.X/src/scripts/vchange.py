#!/usr/bin/env python

import sys, argparse
from configfile import ConfigFile
from version import Version

VERSION_FILE = "./config/version.json"

def main(argv): 
    parser = argparse.ArgumentParser("Controls versioning of flight firmware.")
    parser.add_argument("-j", "--json", default=VERSION_FILE, metavar="FNAME", 
        help="location of config file (JSON format)")
    parser.add_argument("-i", "--inc", choices=["maj", "min", "rev", "build"], default="none", 
        help="increment major, minor, or revision version numbers")
    parser.add_argument("-v", "--verbose", action="store_true", help="turn on verbose output")
    parser.add_argument("-g", "--get", action="store_true", help="gets the current version string")
    
    return build(parser.parse_args(args=argv))

def build(args):
    """Increments version number."""
    
    vobj = ConfigFile(VERSION_FILE)
    version = Version(vobj)

    if args.inc:

        inc(args.inc, version, args.verbose)
        vstr = str(version)

        vobj.set(version.getObject())

    vobj.save()

    if args.get:
        print "Current Version:", str(version)

    return vstr

def inc(val, version, verbose):
    """Increments the version number in the config file."""

    if verbose:
        print "Current Version:", str(version)

    version.inc(val)

    if verbose:
        print "New Version:    ", str(version)


# For running from console.
if __name__ == "__main__":
    main(sys.argv[1:])