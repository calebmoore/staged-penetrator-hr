
ADC_RES = 12

def format_velocity_m_s(v):

    return int(100*v)

def format_velocity_f_s(v):

    return format_velocity_m_s(ft_to_m(float(v)))

def format_acceleration_g(g):

    return int(100*float(g))

def pressure_psi_from_alt_m(a):
    """Calculating pressure from the passed altitude, as outlined here:
    <http://psas.pdx.edu/RocketScience/PressureAltitude_Derived.pdf>
    """

    P0 = 101325  # pressure at zero altitude, Pa
    T0 = 288.15  # temperature at zero altitude, K
    g = 9.80665  # acceleration due to gravity, m/s**2
    L = -6.5 / 1000 # lapse rate, K/m
    R = 287.053  # gas constant for air, J/(kg*K)

    P = P0 * (L*(T0 / L + a)/T0)**(-g/L/R)

    return pa_to_psi(P)

def ft_to_m(x):
    return float(x)*0.3048

def pa_to_psi(x):
    return x / 6894.75728034

def psi_to_v(x):
    """See transfer function and examples in ASDX Pressure Sensor datasheet."""
    return ((0.8*5.0)/15.0)*x + 0.1*5.0

def v_to_adc(x):
    return int(x * (2**ADC_RES - 1) / 5)