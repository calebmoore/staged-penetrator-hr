#!/usr/bin/env python

import sys, argparse, vchange
from headerfile import HeaderFile
from configfile import ConfigFile
from version import Version
from rocketmath import *

CONFIG_FILE = "./config/config.json"
ROCKET_FILE = "./config/rocket.json"

BUILD_HEADER_LOC = "../config/build.h"
BUILD_HEADER_ROCKET_NAME = "ROCKET_NAME"
BUILD_HEADER_LAUNCH_SITE = "LAUNCH_SITE"
BUILD_HEADER_VERSION_STR = "VERSION_STR"
BUILD_HEADER_IS_PENETRATOR = "IS_PENETRATOR"
BUILD_HEADER_IS_TIPOVER = "IS_TIPOVER"
BUILD_HEADER_IGNORE_VELOCITY = "IGNORE_VELOCITY"
BUILD_HEADER_ASCII_LOGGING = "ASCII_LOGGING"
BUILD_HEADER_EJECT_BOOSTER = "EJECT_BOOSTER"

THRES_HEADER_LOC = "../config/thresholds.h"
THRES_HEADER_LAUNCH_ACCEL = "LAUNCH_ACCELERATION"
THRES_HEADER_MIN_VELOCITY = "MINIMUM_VELOCITY"
THRES_HEADER_MIN_IGN_ALT = "MINIMUM_IGN_ALT"
THRES_HEADER_MAX_IGN_ALT = "MAXIMUM_IGN_ALT"
THRES_HEADER_MIN_ARM_ALT = "MINIMUM_ARM_ALT"
THRES_HEADER_MAIN_CHUTE_ALT = "MAIN_CHUTE_ALT"
THRES_HEADER_APOG_DETECT = "APOGEE_DETECT_COUNT"
THRES_HEADER_MECO_DETECT = "MECO_DETECT_COUNT"
THRES_HEADER_MAX_SAFE_VELOCITY = "MAX_SAFE_VELOCITY"
THRES_HEADER_APOG_DELTA_Z = "APOGEE_DELTA_Z"

TIMING_HEADER_LOC = "../config/timing.h"
TIMING_HEADER_LOG_PERIOD = "LOG_PERIOD"
TIMING_HEADER_MAX_IGN_DELAY = "MAX_IGN_DELAY"
TIMING_HEADER_MIN_IGN_DELAY = "MIN_IGN_DELAY"
TIMING_HEADER_APOG_DETECT_PERIOD = "APOGEE_DETECT_PERIOD"
TIMING_HEADER_MECO_DETECT_PERIOD = "MECO_DETECT_PERIOD"
TIMING_HEADER_PYRO_TIMEOUT = "PYRO_TIMEOUT"
TIMING_HEADER_2ND_STAGE_IGN_DELAY = "SECOND_STAGE_IGN_DELAY"
TIMING_HEADER_CONSTANT_IGN_DELAY = "CONSTANT_IGN_DELAY"
TIMING_HEADER_VELOCITY_CHECK_DELAY = "VELOCITY_CHECK_DELAY"
TIMING_HEADER_DATA_SAMPLING_PERIOD = "DATA_SAMPLING_PERIOD"
TIMING_HEADER_BOOSTER_EJECT_DELAY = "BOOSTER_EJECT_DELAY"

def main(argv):
    parser = argparse.ArgumentParser("Runs pre-build routines for penetrator firmware. " + 
        "Configuration file is stored in " + CONFIG_FILE)
    parser.add_argument("-c", "--config", default=CONFIG_FILE, metavar="FILE", 
        help="location of config file (JSON format)")
    parser.add_argument("-b", "--build", type=int, choices=[0,1,2,3], default=0, 
        help="build type: 0=standard flight, 1=penetrator, 2=penetrator test, 3=penetrator with booster eject")
    parser.add_argument("-d", "--debug", type=int, choices=[0,1], default=0, 
        help="debug level: 0=production, 1=no velocity checks")
    parser.add_argument("-i", "--inc", choices=["maj", "min", "rev"], default="none", 
        help="increment major, minor, or revision version numbers")
    parser.add_argument("-v", "--verbose", action="store_true", help="turn on verbose output")
    
    build(parser.parse_args())


def build(args):
    """Increments version number, calculates flight thresholds, and sets the proper build mode."""

    if args.verbose:
        print "Extracting configuration..."
    
    config = ConfigFile(args.config)

    if args.verbose:
        print "Incrementing build number..."
    
    # increment build number
    vstr = vchange.main(["--get", "-v", "-i", "build"])

    if args.verbose:
        print "Building build.h..."

    write_build_file(args, config, vstr)

    if args.verbose:
        print "Doing math..."
    
    do_math(config)

    # config.save()

def write_build_file(args, config, vstr):
    """Writes the proper macros to the build file."""

    build_file = HeaderFile(BUILD_HEADER_LOC)

    build_file.set(BUILD_HEADER_VERSION_STR, vstr)
    build_file.set(BUILD_HEADER_ROCKET_NAME, config["rocket"]["name"])

    # determine which flight profile is desired
    if args.build == 1 or args.build == 3:
        build_file.define(BUILD_HEADER_IS_PENETRATOR)
    elif args.build == 2:
        build_file.define(BUILD_HEADER_IS_PENETRATOR)
        build_file.define(BUILD_HEADER_IS_TIPOVER)
    else:
        build_file.undefine(BUILD_HEADER_IS_PENETRATOR)
        build_file.undefine(BUILD_HEADER_IS_TIPOVER)

    if args.build == 3:
        build_file.define(BUILD_HEADER_EJECT_BOOSTER)
    else:
        build_file.undefine(BUILD_HEADER_EJECT_BOOSTER)

    # determine the debugging level
    if args.debug == 1:
        build_file.define(BUILD_HEADER_IGNORE_VELOCITY)
        build_file.define(BUILD_HEADER_ASCII_LOGGING)
    else:
        build_file.undefine(BUILD_HEADER_IGNORE_VELOCITY)
        build_file.undefine(BUILD_HEADER_ASCII_LOGGING)

    # build header file
    build_file.save()

def do_math(config):
    # extract config info
    thresholds = config["thresholds"]
    timing = config["timing"]

    th_file = HeaderFile(THRES_HEADER_LOC)
    tm_file = HeaderFile(TIMING_HEADER_LOC)

    # threshold calculation / setting
    th_file.set(THRES_HEADER_LAUNCH_ACCEL, 
               format_acceleration_g(thresholds["launchAccel"]))
    th_file.set(THRES_HEADER_MIN_VELOCITY, 
               format_velocity_f_s(thresholds["minimumVelocity"]))
    th_file.set(THRES_HEADER_MAX_SAFE_VELOCITY, 
               format_velocity_f_s(thresholds["maximumSafeVelocity"]))
    th_file.set(THRES_HEADER_APOG_DETECT, int(thresholds["apogeeDetectCount"]))
    th_file.set(THRES_HEADER_MECO_DETECT, int(thresholds["mecoDetectCount"]))

    th_file.set(THRES_HEADER_MIN_IGN_ALT, thresholds["minIgnAltitude"])
    th_file.set(THRES_HEADER_MAX_IGN_ALT, thresholds["maxIgnAltitude"])
    th_file.set(THRES_HEADER_MIN_ARM_ALT, thresholds["minArmAltitude"])
    th_file.set(THRES_HEADER_MAIN_CHUTE_ALT, thresholds["mainChuteAltitude"])
    th_file.set(THRES_HEADER_APOG_DELTA_Z, thresholds["apogeeDeltaZ"])

    tm_file.set(TIMING_HEADER_APOG_DETECT_PERIOD, int(100*timing["apogeeCheckPeriod"]))
    tm_file.set(TIMING_HEADER_PYRO_TIMEOUT, int(100*timing["pyroTimeout"]))
    tm_file.set(TIMING_HEADER_LOG_PERIOD, int(100*timing["loggingPeriod"]))
    tm_file.set(TIMING_HEADER_MECO_DETECT_PERIOD, int(100*timing["mecoCheckPeriod"]))
    tm_file.set(TIMING_HEADER_MAX_IGN_DELAY, int(100*timing["maxIgnDelay"]))
    tm_file.set(TIMING_HEADER_MIN_IGN_DELAY, int(100*timing["minIgnDelay"]))
    tm_file.set(TIMING_HEADER_CONSTANT_IGN_DELAY, int(100*timing["constantIgnDelay"]))
    tm_file.set(TIMING_HEADER_2ND_STAGE_IGN_DELAY, int(100*timing["secondStageIgnDelay"]))
    tm_file.set(TIMING_HEADER_VELOCITY_CHECK_DELAY, int(100*timing["velocityCheckDelay"]))
    tm_file.set(TIMING_HEADER_DATA_SAMPLING_PERIOD, int(100*timing["dataSamplingPeriod"]))
    tm_file.set(TIMING_HEADER_BOOSTER_EJECT_DELAY, int(100*timing["boosterEjectDelay"]))

    # save to C code
    th_file.save()
    tm_file.save()

# For running from console.
if __name__ == "__main__":
    main(sys.argv[1:])