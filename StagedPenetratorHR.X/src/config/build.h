/**
 * build.h
 * 
 * Generated: 2015-03-18 17:33:08.898000 by robot.
 */

#ifndef BUILD_H_
#define BUILD_H_

#define ROCKET_NAME "Triple Port"
#define EJECT_BOOSTER 
#define IS_PENETRATOR 
#define VERSION_STR "2.1.0-20"
#undef IGNORE_VELOCITY 
#undef ASCII_LOGGING 

#endif /* BUILD_H_ */
