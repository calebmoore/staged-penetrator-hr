/**
 * PIC18F2523 fuse settings.
 */

#ifndef CONFIG_H
#define CONFIG_H

// External Oscillator
#pragma config OSC = HS
//#pragma config OSC = INTIO67

// Fail-Safe Clock Monitor Disabled
#pragma config FCMEN = OFF


#pragma config IESO = OFF
#pragma config PWRT = OFF
#pragma config BOREN  = OFF
#pragma config WDT = OFF
#pragma config MCLRE = OFF
#pragma config LPT1OSC = OFF
#pragma config PBADEN = OFF
#pragma config STVREN = OFF
#pragma config LVP = OFF
#pragma config XINST = OFF
#pragma config DEBUG = ON
#pragma config CP0 = OFF
#pragma config CP1 = OFF
#pragma config CP2 = OFF
#pragma config CP3 = OFF
#pragma config CPB = OFF
#pragma config CPD = OFF
#pragma config WRT0 = OFF
#pragma config WRT1 = OFF
#pragma config WRT2 = OFF
#pragma config WRT3 = OFF
#pragma config WRTB = OFF
#pragma config WRTC = OFF
#pragma config WRTD = OFF
#pragma config EBTR0 = OFF
#pragma config EBTR1= OFF
#pragma config EBTR2 = OFF
#pragma config EBTR3 = OFF
#pragma config EBTRB = OFF

#endif  // CONFIG_H
