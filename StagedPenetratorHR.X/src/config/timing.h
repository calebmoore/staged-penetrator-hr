/**
 * timing.h
 * 
 * Generated: 2015-03-18 17:33:08.901000 by robot.
 */

#ifndef TIMING_H
#define TIMING_H

#define DATA_SAMPLING_PERIOD 5
#define PYRO_TIMEOUT 100
#define MIN_IGN_DELAY 700
#define CONSTANT_IGN_DELAY 500
#define MECO_DETECT_PERIOD 10
#define SECOND_STAGE_IGN_DELAY 50
#define VELOCITY_CHECK_DELAY 150
#define APOGEE_DETECT_PERIOD 5
#define LOG_PERIOD 10
#define MAX_IGN_DELAY 1200
#define BOOSTER_EJECT_DELAY 350

#endif /* TIMING_H */
