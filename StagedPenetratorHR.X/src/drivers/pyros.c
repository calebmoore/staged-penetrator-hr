/*
 * pyros.c
 * Caleb Moore
 * 2014-08-25
 *
 * Pyro timing and thresholds can be configured in the config.json file.
 */

#include "pyros.h"
#include "globals.h"
#include "flightevent.h"
#include "timing.h"

// Equivalent to ((1 << ARM) | (1 << IGN) | (1 << JET)) without the compiler
// warning of "shift expression has no effect" since (1 << ARM) evaluates to 1.
#define PYROS_BITMASK_ALL   0x7
#define PYROS_BITMASK(pyro) (1 << (pyro >> 1))

// static prototypes
static void setPyro(int pyro);
static void clearPyro(int pyro);
static void clearAllPyros(void);
static int16_t stopIgnition(void);
static int16_t stopJettison(void);

// flight events
static FlightEvent timeoutIgn;
static FlightEvent timeoutJet;

/**
 * Configures outputs for the pyro channels and initializes pyro-related
 * flight events.
 */
void pyrosInit() {
    // clear all pyros, configure outputs
    REG_HIGH(LATB, PYROS_BITMASK_ALL);
    REG_LOW(TRISB, PYROS_BITMASK_ALL);

    initializeFlightEvent(&timeoutIgn, PYRO_TIMEOUT, stopIgnition);
    initializeFlightEvent(&timeoutJet, PYRO_TIMEOUT, stopJettison);

    flightState.pyroState = 0;
}

/**
 * Electrically connects the firing circuitry to the pyro igniters.
 *
 * @return 0
 */
int16_t armPyros() {
    setPyro(PYROS_ARM);
    flags.pyrosArmed = 1;

    return FLIGHT_EVENT_INACTIVE;
}

/**
 * Electrically disconnects the firing circuitry from any igniters, prohibiting
 * ignition.
 *
 * @return 0
 */
int16_t disarmPyros() {
    if (flightState.pyroState != PYROS_ARM) {
        clearAllPyros();
    } else {
        clearPyro(PYROS_ARM);
    }
  
    flags.pyrosArmed = 0;

    return FLIGHT_EVENT_INACTIVE;
}

/**
 * If the pyros are armed, fires the igniters for the outboard motors. A
 * configurable amount of time later, the pyro channel is closed.
 *
 * @return message to deactivate event
 */
int16_t igniteMotors() {
    if (!flags.pyrosArmed)
        return 1;

    setPyro(PYROS_IGN);
    flags.ignited = 1;

    eventTimerSubscribe(&timeoutIgn);

    return FLIGHT_EVENT_INACTIVE;
}

/**
 * If the pyros are armed, ignites the charge to jettison the computer. Two
 * seconds after ignition, the pyro channel is closed.
 *
 * The length of time the pyro channel is active is configurable in the macro
 * definitions at the top of this file (PYROS_TIMEOUT).
 *
 * @return message to deactivate event
 */
int16_t jettisonComputer(void) {
    if (!flags.pyrosArmed)
        return 1;

    setPyro(PYROS_JET);
    flags.jettisoned = 1;

    eventTimerSubscribe(&timeoutJet);

    return FLIGHT_EVENT_INACTIVE;
}

/**
 * Closes the corresponding relay and sets the status bit of the passed pyro
 * channel.
 *
 * @param pyro channel to fire
 */
static void setPyro(int pyro) {
    REG_LOW(LATB, PYROS_BITMASK(pyro)); // flip relay
    flightState.pyroState |= pyro;
}

/**
 * Opens the corresponding relay and clears the status bit of the passed pyro
 * channel.
 *
 * @param pyro channel to fire
 */
static void clearPyro(int pyro) {
    REG_HIGH(LATB, PYROS_BITMASK(pyro));
    flightState.pyroState &= ~pyro;
}

/**
 * Opens all pyro relays, clears all status bits.
 */
static void clearAllPyros() {
    REG_HIGH(LATB, PYROS_BITMASK_ALL);
    flightState.pyroState = 0;
}

/**
 * Event callback that closes the pyro channel used to ignite the outboard
 * motors.
 *
 * @return message to disable the event
 */
static int16_t stopIgnition() {
    clearPyro(PYROS_IGN);
    return FLIGHT_EVENT_INACTIVE;
}

/**
 * Event callback that closes the pyro channel used to jettison the computer.
 *
 * @return message to disable the event
 */
static int16_t stopJettison() {
    clearPyro(PYROS_JET);
    return FLIGHT_EVENT_INACTIVE;
}
