/* 
 * File:   buzzer.h
 * Author: Winglee Plasma Lab
 *
 * Created on November 20, 2014, 5:55 PM
 */

#ifndef BUZZER_H
#define	BUZZER_H

#include <xc.h>

#define BUZZER_TRIS TRISB
#define BUZZER_PORT PORTB
#define BUZZER_LAT  LATB
#define BUZZER_PIN  3

void buzzerInit(void);
void buzzerEnable(void);
void buzzerDisable(void);
void buzzerPlaySequence(void);

#endif	/* BUZZER_H */

