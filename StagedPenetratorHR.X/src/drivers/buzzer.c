#include "buzzer.h"
#include <stdint.h>
#include <delays.h>
#include "eventtimer.h"
#include "flightevent.h"
#include "flightstate.h"
#include "globals.h"
#include "pyros.h"
#include "beep.h"

static uint8_t _isEnabled;

static Beep *_currentSeq;

static FlightEvent _eventBeep;
static int16_t _beep(void);

static uint8_t getSeq(void);

void buzzerInit() {

    BEEP_OFF();
    BUZZER_TRIS &= ~(1 << BUZZER_PIN); // set buzzer pin as output
    
    _currentSeq = (Beep*)_seqInit;

    buzzerEnable();
    buzzerPlaySequence();

    initializeFlightEvent(&_eventBeep, 1, _beep);
    eventTimerSubscribe(&_eventBeep);
}

void buzzerEnable() {
    _isEnabled = 1;
}

void buzzerDisable() {
    _isEnabled = 0;
}

/**
 * Synchronously plays the current sequence.
 *
 * This is a blocking function! Beep sequences take a looooong time. Be careful!
 *
 * Note, this will play the sequence once, regardless of whether it's defined to
 * repeat.
 */
void buzzerPlaySequence() {
    Beep *seq = _currentSeq;
    Beep b;
    uint8_t i, j;

    for (i = 0; ; i++) {
        b = seq[i];

        if (b.beepType == BEEP_TYPE_CMD)
            break;
        else if (b.beepType == BEEP_TYPE_ON)
            BEEP_ON();
        else if (b.beepType == BEEP_TYPE_OFF)
            BEEP_OFF();

        j = b.beepLength;
        while (j--)
            __delay_ms(10);
    }

    BEEP_OFF();
}

static int16_t _beep() {
    static uint8_t seqIndex = 0;
    static uint8_t isStopped = 0;
    Beep b;

    // update beep sequence, reset index if needed
    if (getSeq()) {
        seqIndex = 0;
        isStopped = 0;
    }

    // check if buzzer is enabled and the sequence is set to repeat
    if (!_isEnabled || isStopped) {
        BEEP_OFF();
        return FLIGHT_EVENT_CONTINUE;
    }

    // make beeps
    b = _currentSeq[seqIndex++];
    switch (b.beepType) {
        case BEEP_TYPE_ON:
            BEEP_ON();
            break;
        case BEEP_TYPE_OFF:
            BEEP_OFF();
            break;
        case BEEP_TYPE_CMD:
            if (b.beepLength == BEEP_SEQ_STOP) {
                isStopped = 1;
                BEEP_OFF();
            } else {
                seqIndex = 0;
            }
        default:
            return 1;
    }
    
    // how long to beep
    return b.beepLength;
}

static uint8_t getSeq() {
    static uint8_t pyroMask = PYROS_IGN | PYROS_JET;

    Beep *last = _currentSeq;
    
    switch (flightState.status) {
        case kFlightStatusInit:
            _currentSeq = _seqInit;
            break;
        case kFlightStatusOnPad:
            _currentSeq = _seqOnPad;
            break;
        case kFlightStatusCoast:
        case kFlightStatusApogee:
        case kFlightStatusIgnition:
            if (flags.pyrosArmed) {
                if (flightState.pyroState & pyroMask)
                    _currentSeq = _seqFire;
                else
                    _currentSeq = _seqArmed;
            } else {
                _currentSeq = _seqNone;
            }
            break;
        case kFlightStatusJettisoned:
            _currentSeq = _seqFindMe;
            break;
        default:
            _currentSeq = _seqNone;
    }

    return last != _currentSeq;
}