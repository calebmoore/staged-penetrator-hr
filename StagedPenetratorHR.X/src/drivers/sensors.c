#include "sensors.h"
#include "globals.h"
#include "flightevent.h"
#include "eventtimer.h"
#include "math.h"
#include "thresholds.h"
#include "timing.h"
#include <adc.h>

#define ACCEL_G_IN_MV            36 // 0.036 V/g for this accelerometer

/**
 * Formats a number 0-12 into what the PIC expects as a channel number when
 * using the SetChanADC() function. This is used to allow looping through
 * ADC channels programatically without needing to use ADC_CH0, ADC_CH1, etc.
 *
 * @param ch channel number to format, 0-12
 */
#define SENSORS_CHANNEL(ch)      (uint8_t)((ch << 3) | 0b10000111)

/**
 * Passed to the OpenADC() function, this selects which analog inputs to use
 * on the PIC.
 */
#define SENSORS_CHANNEL_CONFIG   ADC_2ANA

/**
 * Global access to sensor data.
 */
volatile struct Sensors sensors;

static uint8_t  _currentChannel;
static uint32_t _averagingData[SENSORS_N_TOTAL] = { 0 };
static uint32_t _oversampleSum;
static uint8_t  _oversamples;

// events
static FlightEvent _takeData;

// event callbacks
static int16_t takeData(void);

/**
 * Starts an ADC conversion on the passed channel.
 *
 * @param channel the ADC channel 0-12
 */
static void startConversion(uint8_t channel);

/**
 * 
 * @param sample
 * @return 
 */
static uint16_t decimateSample(uint32_t sample);

/**
 * Processes just-sampled data, doing unit conversions, velocity integration,
 * global updating.
 */
static void processData(void);

/**
 * Initializes the sensors and opens the ADC module.
 */
void sensorsInit() {
    // clear pending ADC interrupts
    PIR1bits.ADIF = 0;

    // initialize ADC module
    OpenADC(ADC_FOSC_16       &
            ADC_RIGHT_JUST   &
            ADC_12_TAD,
            SENSORS_CHANNEL(SENSORS_ACCELEROMETER) &
            ADC_INT_ON       &
            ADC_REF_VDD_VSS,
            SENSORS_CHANNEL_CONFIG);

    // take initial data
    sensors.data[SENSORS_ACCELEROMETER] =
        sensors.lastData[SENSORS_ACCELEROMETER] =
        sensorsReadChannel(SENSORS_ACCELEROMETER);
    
    sensors.data[SENSORS_PRESSURE] = 
        sensors.lastData[SENSORS_PRESSURE] = 
        sensorsReadChannel(SENSORS_PRESSURE);

    _currentChannel = SENSORS_ACCELEROMETER;
    _oversampleSum = 0;

    // set up sensor sampling event
    initializeFlightEvent(&_takeData, 1, takeData);
    eventTimerSubscribe(&_takeData);
}

/**
 * Begins the conversion process.
 */
void sensorsReadAll() {
    startConversion(_currentChannel);
}

/**
 * Adds the sampled value to a running sum, then divides it by the proper
 * decimation value to increase effective ADC resolution.
 */
void sensorsHandleSample() {
    _oversampleSum += ReadADC();

    // keep ordering conversions until sufficiently oversampled
    if (++_oversamples < ADC_OVERSAMPLES) {
        ConvertADC();
    } else {
        flags.adcConversionDone = 1;
        _oversamples = 0;
    }
}

/**
 * Stores the result of the last ADC conversion and starts the next one, unless
 * all sensor conversions are done for this event. Simply checking the status
 * of the channel index is good enough to figure out if sampling is done.
 *
 * Also handles averaging of samples to smooth out noise.
 */
void sensorsHandleFullConversion() {
    static int sampleCount = 1;
    uint16_t sample = decimateSample(_oversampleSum);
    _oversampleSum = 0;

    _averagingData[_currentChannel] += sample;

    if (sampleCount >= DATA_SAMPLING_PERIOD) {
        // store previous data, average new samples
        sensors.lastData[_currentChannel] = sensors.data[_currentChannel];
        sensors.data[_currentChannel] = 
                _averagingData[_currentChannel] / DATA_SAMPLING_PERIOD;
        _averagingData[_currentChannel] = 0;
    }

    _currentChannel++;

    if (_currentChannel >= SENSORS_N_TOTAL) {
        // reset sensor index
        _currentChannel = SENSORS_ACCELEROMETER;

        if (sampleCount++ >= DATA_SAMPLING_PERIOD)
        {
            // all sensors have been sampled, process the data
            sampleCount = 1;
            processData();
        }
    } else {
        startConversion(_currentChannel);
    }
}

/**
 * Samples a single sensor.
 *
 * This is a blocking function.
 *
 * @param channel sensor to sample
 */
uint16_t sensorsReadChannel(uint8_t channel) {
    uint32_t val;
    int i = 0;

    ADC_INT_DISABLE(); // disable ADC interrupt

    val = 0;
    for (i = 0; i < ADC_OVERSAMPLES; i++) {
        startConversion(channel);
        while(BusyADC());
        val += ReadADC();
    }

    // reenable ADC interrupt and clear interrupt flag
    PIR1bits.ADIF = 0;
    ADC_INT_ENABLE();

    return decimateSample(val);
}

/**
 * Formats the passed channel into what the PIC wants, and begins a conversion.
 *
 * @param channel sensor to sample
 */
static void startConversion(unsigned char channel) {
    SelChanConvADC(SENSORS_CHANNEL(channel));
}

/**
 * 
 * @param sample
 * @return
 */
static uint16_t decimateSample(uint32_t sample) {
    return (uint16_t)(sample >> ADC_RESOLUTION_INCREASE);
}

/**
 * Flight event to sample all sensors.
 *
 * @return message to keep same period
 */
static int16_t takeData() {
    sensorsReadAll();
    return FLIGHT_EVENT_CONTINUE;
}

/**
 * Perform math and fill flightState object with new data.
 */
static void processData() {
    int32_t lastxl, xl, pres, min, estPres, estMin, estAp;

    lastxl = flightState.acceleration;
    xl = (int32_t)sensors.data[SENSORS_ACCELEROMETER] - (int32_t)flightState.oneGee;
    pres = (int32_t)sensors.data[SENSORS_PRESSURE];
    estPres = flightState.estimatedPressure;
    min = flightState.minimumPressure;
    estMin = flightState.estimatedMinimumPressure;
    estAp = flightState.estimatedApogee;

    getAccelerationFromADC(&xl);

    flightState.acceleration = xl;
    flightState.pressure = pres;
    flightState.minimumPressure = pres < min ? pres : min;
    flightState.altitude = getAltitudeFromPressure(pres) - flightState.elevation;

    // use recursive filter to estimate real pressure
    flightState.estimatedPressure = estPres + (pres - estPres) / 5;
    flightState.estimatedMinimumPressure = estPres < estMin ? estPres : estMin;
    flightState.estimatedAltitude = getAltitudeFromPressure(estPres) - flightState.elevation;
    flightState.estimatedApogee = flightState.estimatedAltitude > estAp
        ? flightState.estimatedAltitude : estAp;

    // Velocity Calculation
    // Acceleration is stored in 100ths of a G. When integrated with time
    // measured in ms, the necessary conversion is 9.81/1000 in order to
    // get 100ths of a m/s.
    if (flags.launched) {
        fractionalIntegrate(&flightState.velocity, lastxl, xl, 
                            DATA_SAMPLING_PERIOD*10L,
                            MATH_GEE_IN_MS2_X100,
                            MATH_INTEGRATION_SCALE_FACTOR);
    }

    flags.safeVelocity = flightState.velocity > (int32_t)MAX_SAFE_VELOCITY ? 0 : 1;
}

