/**
 * pyros.h
 * Caleb Moore
 * 2014-08-25
 *
 * Controls arming, disarming, and firing of onboard pyrotechnics, including the
 * outboard motors and flight computer jettison.
 *
 * Most behavior is event driven, using the event timer to time activation and
 * deactivation of relays.
 *
 * Call pyrosInit() to setup outputs. Then igniteMotors() and jettisonComputer()
 * are ready to work. They will only fire if pyros are armed, both electrically
 * and in software. This is done with armPyros().
 *
 * The current state of the pyros can be seen in flightState.pyroState. It's an
 * 8-bit state flag with the following configuration:
 * 
 *            4     2     1
 *     MSB [JET] [IGN] [ARM] LSB
 *
 *     ie: pyroState = 7, all relays closed (all pyros firing)
 *         pyroState = 1, pyros are armed, none firing
 *         pyroState = 2, ignition pyro firing, but disarmed
 *
 * Additionally, flags.pyrosArmed is a globally available flag.
 *
 * All public functions can be used as FlightEventCallbacks.
 */

#ifndef PYROS_H
#define	PYROS_H

#define PYROS_ARM 1
#define PYROS_IGN 2
#define PYROS_JET 4

#include <xc.h>
#include <stdint.h>

/**
 * Configures chip outputs and flight events.
 */
void pyrosInit(void);

/**
 * Connects the firing circuity to power.
 *
 * @return message to deactivate event
 */
int16_t armPyros(void);

/**
 * Disconnects the firing circuitry.
 *
 * @return message to deactivate event
 */
int16_t disarmPyros(void);

/**
 * Fires the igniters in the outboard motors.
 *
 * @return message to deactivate event
 */
int16_t igniteMotors(void);

/**
 * Fires the igniter attatched to the auxilliary channel.
 *
 * @return message to deactivate event
 */
int16_t jettisonComputer(void);

#endif	/* PYROS_H */
