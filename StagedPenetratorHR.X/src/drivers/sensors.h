/* 
 * sensors.h
 * Caleb Moore
 * 2014-08-08
 *
 * This module is built to service many ADC channels in rapid succession.
 * For the penetrator flights, only two are used, however.
 */

#ifndef SENSORS_H
#define	SENSORS_H

#include <xc.h>
#include <stdint.h>

/**
 * Oversampling and decimation values to increase effective resolution of ADC.
 */
#define ADC_RESOLUTION_INCREASE (2) // bits of additional resolution desired
#define ADC_OVERSAMPLES         (1 << ADC_RESOLUTION_INCREASE)*(1 << ADC_RESOLUTION_INCREASE)
#define ADC_EQU_RESOLUTION      (12 + ADC_RESOLUTION_INCREASE)
#define ADC_REFERENCE_V         5

/**
 * Indices for the various sensors used.
 */
#define SENSORS_ACCELEROMETER 0
#define SENSORS_PRESSURE      1
#define SENSORS_BATTERY       2

/**
 * Total number of sensors onboard.
 */
#define SENSORS_N_TOTAL       2

/**
 * Number of samples (and thus event ticks) between averages.
 */
#define SENSORS_N_AVG_SAMPLES 10

/**
 * Holds the current sensor values.
 */
struct Sensors {
    uint16_t data[SENSORS_N_TOTAL];
    uint16_t lastData[SENSORS_N_TOTAL];
};

extern volatile struct Sensors sensors;

/**
 * Opens the ADC module and configures data structures.
 */
void sensorsInit(void);

/**
 * Asynchronously samples all sensors.
 */
void sensorsReadAll(void);

/**
 * Synchronously samples one sensor.
 *
 * @param channel sensor to sample
 * @return value of conversion
 */
uint16_t sensorsReadChannel(uint8_t channel);

/**
 * Processes an ADC conversion and prepares the next one, if necessary.
 */
void sensorsHandleFullConversion(void);

/**
 * Processes and individual ADC sample and prepares to continue sampling or
 * decimate.
 */
void sensorsHandleSample(void);

#endif	/* SENSORS_H */

